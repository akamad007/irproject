package edu.buffalo.cse.irf14.index;

import java.util.HashMap;

public class CaseInsensitiveMap extends HashMap<String, Integer> {

    @Override
    public Integer put(String key, Integer value) {
    	
    	return super.put(key.toLowerCase(), value);
    }

    // not @Override because that would require the key parameter to be of type Object
    public Integer get(String key) {
    	
       return super.get(key.toLowerCase());
    }
}