/**
 * 
 */
package edu.buffalo.cse.irf14.index;


import edu.buffalo.cse.irf14.document.Parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.HashSet;


/**
 * @author nikhillo
 * Class that emulates reading data back from a written index
 */
public class IndexReader {
	/**
	 * Default constructor
	 * @param indexDir : The root directory from which the index is to be read.
	 * This will be exactly the same directory as passed on IndexWriter. In case 
	 * you make subdirectories etc., you will have to handle it accordingly.
	 * @param type The {@link IndexType} to read from
	 */
	
	
	
	private String [] indexName;
	private static HashMap<Integer, Integer> documentLengthDictionary = null;
	private static HashMap<Integer, String> documentDictionary,reverseDictionary = null;
	private static Integer documentAverageLength = null;
	private static HashMap<Integer,HashMap<Integer,Integer>> inverseDocumentTermDictionary = null;
	
	
	private CaseInsensitiveMap dictionary;
	private HashMap<String,HashMap<Integer,Integer>> index;
	static String pattern = System.getProperty("file.separator");
	
	IndexType type;
	final int BUCKETS = 27;

	public IndexReader(String indexDir, IndexType type) {
		//TODO						
		this.type = type;
		
		if(documentDictionary==null){
			String s = indexDir+pattern+"document";
			documentDictionary = IndexReader.loadDocumentDictionary(s);
			documentLengthDictionary = IndexReader.loadDocumentLengthDictionary(s);
			documentAverageLength = IndexReader.loadDocumentAverageLength(s);
			inverseDocumentTermDictionary = IndexReader.loadInverseDocumentTermDict(s);
		}
		switch(type){
		case TERM:	
			dictionary = setIndexDictionaryName(indexDir,"term");		
			
			break;
		case AUTHOR:
			dictionary = setIndexDictionaryName(indexDir,"author");			
			break;
		case CATEGORY:
			dictionary = setIndexDictionaryName(indexDir,"category");			
			break;
		case PLACE:
			dictionary = setIndexDictionaryName(indexDir,"place");			
			break;
		default:
			break;		
		}		
	}
	
	private Integer getTermBucket(String s){
		if(s!=null&&!s.isEmpty())
		{char t = s.toUpperCase().charAt(0);		
		int value = Character.getNumericValue(t)-10;
		if(value>=BUCKETS||value<0)
			value = BUCKETS-1;
		return value;}
		return BUCKETS-1;
	}
	private CaseInsensitiveMap setIndexDictionaryName(String dir, String s){				
		String path = dir+pattern+s;
		File indexDirectory = new File(path);
		File [] indexes = indexDirectory.listFiles();
		Arrays.sort(indexes);
		CaseInsensitiveMap t = null;
		indexName = new String [indexes.length-1];		
		int i = 0;
		for(File index:indexes){	
			BufferedReader br;
			try {
				
				br = new BufferedReader(new FileReader(index.getAbsoluteFile()));				
						if(index.getName().contains("Index")){	
							if (br.readLine() != null) {
								indexName[i] = index.getAbsolutePath();								
							}
							else{
								indexName[i] = null;
							}
							i++;				
						}
						else if(index.getName().contains("Dict")){				
							t = loadDictionary(index.getAbsolutePath());						
						}						
			br.close();
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}     
			
			
		}	
		
		//Logic for indexes etc		
		return t;
	}
	private CaseInsensitiveMap loadDictionary(String s){
		CaseInsensitiveMap mapInFile=new CaseInsensitiveMap();
		reverseDictionary = new HashMap< Integer,String>();
		 try{
			 BufferedReader br =  new BufferedReader(new FileReader(s));	    	      
		        //read data from file line by line:
		        String currentLine;
		        while((currentLine=br.readLine())!=null){	   		            
		            //now tokenize the currentLine:
		            StringTokenizer st=new StringTokenizer(currentLine,"=",false);		             
		            //put tokens ot currentLine in map	
		            String key =st.nextToken();
		            int value = Integer.parseInt(st.nextToken());
		        
		            mapInFile.put(key,value);
		            reverseDictionary.put(value, key);
		        }		        
		        br.close();		        
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		return mapInFile;
	}
	
	
	private static HashMap<Integer,String> loadDocumentDictionary(String s){
		
		HashMap<Integer,String> mapInFile=new HashMap< Integer,String>();
		String indexes = s+pattern+"documentDict.txt";
		 try{
				BufferedReader br =  new BufferedReader(new FileReader(indexes));	    	      
		        //read data from file line by line:
		        String currentLine;
		        while((currentLine=br.readLine())!=null){	            
		            //now tokenize the currentLine:
		            StringTokenizer st=new StringTokenizer(currentLine,"=",false);		             
		            //put tokens ot currentLine in map	
		            String value = st.nextToken();
		            int key = Integer.parseInt(st.nextToken());
		            mapInFile.put(key,value);
		        }
		        br.close();		        
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		return mapInFile;
	}
	
private static HashMap<Integer,Integer> loadDocumentLengthDictionary(String s){
		
		HashMap<Integer, Integer> mapInFile=new HashMap< Integer,Integer>();
		
		String indexes = s+pattern+"documentLengthDict.txt";
		 try{
			 BufferedReader br =  new BufferedReader(new FileReader(indexes));	      
		        //read data from file line by line:
		        String currentLine;		       
		        while((currentLine = br.readLine())!=null){
		            		            
		            //now tokenize the currentLine:
		            StringTokenizer st=new StringTokenizer(currentLine,"=",false);		             
		            //put tokens ot currentLine in map			           
		            int key = Integer.parseInt(st.nextToken());
		            int value =Integer.parseInt( st.nextToken());
		            mapInFile.put(key,value);		            
		        }		     
		      br.close();		        
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		
		return mapInFile;
	}

public static Integer getTotalDocuments(){
	return documentDictionary.size();
}

private static HashMap<Integer, HashMap<Integer, Integer>> loadInverseDocumentTermDict(String s){
	String indexes = s+pattern+"inverseDocumentTermDict.txt";
	HashMap<Integer,HashMap<Integer,Integer>> mapInFile = new HashMap<Integer,HashMap<Integer,Integer>>();
	 try{
		 BufferedReader br =  new BufferedReader(new FileReader(indexes));	      
	        //read data from file line by line:
	        String currentLine;		       
	        while((currentLine = br.readLine())!=null){
	            		            
	        	HashMap<Integer, Integer> integerMap = new HashMap<Integer, Integer>();
	            
	            //now tokenize the currentLine:
	            StringTokenizer st= new StringTokenizer(currentLine,"=",false);
	            StringTokenizer sn ;
	            String [] numberMap = currentLine.split("\\{")[1].split("\\}")[0].split(",");		            
	            //put tokens ot currentLine in map		            		           
	            for(String num:numberMap)
	            {		    		            	
	            	sn = new StringTokenizer(num.trim(),"=",false);			            	
	            	integerMap.put(Integer.parseInt(sn.nextToken()), Integer.parseInt(sn.nextToken()));		            			            	
	            }		            		           		            
	            mapInFile.put(Integer.parseInt(st.nextToken()),integerMap);		            
	        }		     
	      br.close();		        
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	 return mapInFile;
	
}

private static Integer loadDocumentAverageLength(String s){
	Integer avgLength = 0;
	String indexes = s+pattern+"documentAvgLength.txt";
	 try{
		 BufferedReader br;	
		 br = new BufferedReader(new FileReader(indexes));	
		 avgLength = Integer.parseInt(br.readLine());
		 br.close();
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	
	return avgLength;
}

	
	private HashMap<String, HashMap<Integer,Integer>> loadTermIndex(String s){		
		HashMap<String, HashMap<Integer,Integer>> mapInFile=new HashMap<String, HashMap<Integer,Integer>>();
		 try{
			 	BufferedReader br;	
				br = new BufferedReader(new FileReader(s));	      
		        //read data from file line by line:
		        String currentLine;
		        
		        while((currentLine=br.readLine())!=null){
		        	HashMap<Integer, Integer> integerMap = new HashMap<Integer, Integer>();
		            	            		            
		            //now tokenize the currentLine:
		            StringTokenizer st= new StringTokenizer(currentLine,"=",false);
		            StringTokenizer sn ;
		            String [] numberMap = currentLine.split("\\{")[1].split("\\}")[0].split(",");		            
		            //put tokens ot currentLine in map		            		           
		            for(String num:numberMap)
		            {		    		            	
		            	sn = new StringTokenizer(num.trim(),"=",false);			            	
		            	integerMap.put(Integer.parseInt(sn.nextToken()), Integer.parseInt(sn.nextToken()));		            			            	
		            }		            		           		            
		            mapInFile.put(st.nextToken(),integerMap);		           
		        }
		        
		        br.close();		        
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		return mapInFile;
		
	}
	
	/**
	 * Get total number of terms from the "key" dictionary associated with this 
	 * index. A postings list is always created against the "key" dictionary
	 * @return The total number of terms
	 */
	
	public int getTotalKeyTerms() {
		//TODO : YOU MUST IMPLEMENT THIS				
		return dictionary.size();
	}
	
	/**
	 * Get total number of terms from the "value" dictionary associated with this 
	 * index. A postings list is always created with the "value" dictionary
	 * @return The total number of terms
	 */
	public int getTotalValueTerms() {
		//TODO: YOU MUST IMPLEMENT THIS		 
		return documentDictionary.size();
	}
	
	/**
	 * Method to get the postings for a given term. You can assume that
	 * the raw string that is used to query would be passed through the same
	 * Analyzer as the original field would have been.
	 * @param term : The "analyzed" term to get postings for
	 * @return A Map containing the corresponding fileid as the key and the 
	 * number of occurrences as values if the given term was found, null otherwise.
	 */
	public Map<String, Integer> getPostings(String term) {
		//TODO:YOU MUST IMPLEMENT THIS	
		int i = getTermBucket(term);
	
		if(dictionary.get(term)!=null&&indexName[i]!=null){			
			String indexId = dictionary.get(term).toString();			
			HashMap<String,Integer> map = new HashMap<String,Integer>();					
			HashMap<Integer,Integer> documentMap = new HashMap<Integer,Integer>();	
			index = loadTermIndex(indexName[i]);
			documentMap = index.get(indexId);		
			for(Map.Entry<Integer, Integer> m :documentMap.entrySet()){
					int frequency = m.getValue();							
					String docName = documentDictionary.get(m.getKey());										
					map.put(docName,frequency);					
				}	
		
			return map;
		}
		return null;
	}
	
	
	//This gives the term frequency for a particular term.
	public Integer getTermFrequency(String term , int docId) {
		//TODO:YOU MUST IMPLEMENT THIS	
		int i = getTermBucket(term);		
		if(dictionary.get(term)!=null&&indexName[i]!=null){			
			String indexId = dictionary.get(term).toString();			
			HashMap<String,Integer> map = new HashMap<String,Integer>();					
			HashMap<Integer,Integer> documentMap = new HashMap<Integer,Integer>();	
			
			index = loadTermIndex(indexName[i]);
			documentMap = index.get(indexId);				
			return documentMap.get(docId)	;
		}
		return null;
	}
	/**
	 * Method to get the top k terms from the index in terms of the total number
	 * of occurrences.
	 * @param k : The number of terms to fetch
	 * @return : An ordered list of results. Must be <=k fr valid k values
	 * null for invalid k values
	 */
	public List<String> getTopK(int k) {
		//TODO YOU MUST IMPLEMENT THIS
		
		HashMap<String,HashMap<Integer,Integer>> map = new HashMap<String,HashMap<Integer,Integer>>();
		HashMap <Integer,String> result = new HashMap<Integer,String>();
		
		for(int i = 0;i<indexName.length;i++){
			if(indexName[i]!=null){								
				map = loadTermIndex(indexName[i]);				
				for(Entry<String, HashMap<Integer, Integer>> h:map.entrySet()){					
					HashMap<Integer, Integer> temp = h.getValue();
					int total = 0;
					for(Entry<Integer, Integer> t :temp.entrySet()){							
						total = total+t.getValue()	;		
					}
					result.put(total, reverseDictionary.get((Integer.parseInt(h.getKey()))));	
				}
			 }
		}
		
		TreeMap<Integer,String> res = new TreeMap<Integer,String>(result);			
		List<String> list = new ArrayList<String>(res.values());
		Collections.reverse(list);
		
		if(k<list.size()&&k>0){
			list = list.subList(0, k);
		}
		else{
			list = null;
		}
		return list;
	}
	
	
	public ArrayList<Integer> not(ArrayList<Integer> list1,ArrayList<Integer> list2){
		
		if(list1!=null){
			if(list2!=null)
			list1.removeAll(list2);
			HashSet hs = new HashSet();
			hs.addAll(list1);
			list1.clear();
			list1.addAll(hs);	
		}
	return list1;
}
	
	public ArrayList<Integer> intersection(ArrayList<Integer> list1,ArrayList<Integer> list2){
		if(list1!=null){
			if(list2!=null)
				list1.retainAll(list2);
		HashSet hs = new HashSet();
		hs.addAll(list1);
		list1.clear();
		list1.addAll(hs);}	
	return list1;
}
	
public static Integer getAverageDocumentLength(){
	return documentAverageLength;
}
	

public ArrayList<Integer> union(ArrayList<Integer> list1,ArrayList<Integer> list2){
	if(list1!=null){
		if(list2!=null)
		list1.addAll(list2);
		HashSet hs = new HashSet();
		hs.addAll(list1);
		list1.clear();
		list1.addAll(hs);	
		return list1;}
	else 
	{
		return list2;
	}
}
public Integer getDocumentLength(Integer docId){
	
	
	return documentLengthDictionary.get(docId);
	
}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Integer> getDocIdList(String term1) {
			
			if(term1!=null){
			String termId;int j;HashMap<Integer,Integer> a=new HashMap<Integer,Integer>() ;	
			
			if(dictionary.get(term1)!=null){	
				
				termId = dictionary.get(term1).toString();												
				j = getTermBucket(term1);								
				if(indexName[j]!=null)
				a= loadTermIndex(indexName[j]).get(termId);	 
				ArrayList<Integer> result = new ArrayList<Integer>();
				result.addAll(a.keySet());
				return result;				
			}	
			}
			return null;
	}
	public static HashMap<Integer,Integer> getDocumentTermInverse(Integer docId){
		
		
		return inverseDocumentTermDictionary.get(docId);		
	}
	
	public static String[] getDocumentTitleContent( List<String> terms,String docName,String path){
		String filePath = path+pattern+docName;
		String title = ""; 
		String  content= "";
		
		String [] result = new String[2];
		BufferedReader br;	
		try {
				br = new BufferedReader(new FileReader(filePath));						      
		        //read data from file line by line:
		        String currentLine;	
		        Boolean keepLooping = true;
		        while(((currentLine=br.readLine())!=null)&&keepLooping){
		        	if(!currentLine.isEmpty()){						
		        		if(title==""){
							title = Parser.regexTitle(currentLine,br);								
						}     
						
							for(int i=0;i<terms.size();i++){
								
								if(currentLine.toLowerCase().contains(terms.get(i).toLowerCase())){
									
									if(!(content.toLowerCase().contains(currentLine.toLowerCase())))
										{											
											content = content+currentLine+"...\n";
										}	
									
									if(content.length()>150)
										keepLooping = false;
									break;
								
							}
					
						}
		           		           
		        }
		        
		      }
		}
		 catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		result[0] = title;
		result[1] = content;
		
		return result;
		
	}
	public static String getDocumentName(Integer docId){
		return documentDictionary.get(docId);
	}
	
}
