/**
 * 
 */
package edu.buffalo.cse.irf14.index;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Map.Entry;
import java.util.TreeMap;

import edu.buffalo.cse.irf14.analysis.AnalyzerFactory;
import edu.buffalo.cse.irf14.analysis.TokenStream;
import edu.buffalo.cse.irf14.analysis.Tokenizer;
import edu.buffalo.cse.irf14.analysis.TokenizerException;
import edu.buffalo.cse.irf14.document.Document;
import edu.buffalo.cse.irf14.document.FieldNames;

/**
 * @author nikhillo
 * Class responsible for writing indexes to disk
 */
public class IndexWriter {
	/**
	 * Default constructor
	 * @param indexDir : The root directory to be sued for indexing
	 */
	String mainIndexDir;
	final int MAX_STREAM_LIST_SIZE = 1500,BUCKETS = 27;
	
	@SuppressWarnings("rawtypes")
	private HashMap [] authorIndex = new HashMap[BUCKETS],categoryIndex= new HashMap[BUCKETS],placeIndex= new HashMap[BUCKETS],termIndex= new HashMap[BUCKETS];	
	private int documentI = 0, authorI = 0, categoryI = 0, placeI = 0,termI = 0,docLength = 0,docTotalLength = 0;	
	private int writeCountTerm = 0;	
	private CaseInsensitiveMap documentDict = new CaseInsensitiveMap()
	,authorDict = new CaseInsensitiveMap() 
	,termDict  = new CaseInsensitiveMap(),
	categoryDict= new CaseInsensitiveMap()
	,placeDict = new CaseInsensitiveMap();
	private HashMap<Integer,Integer>docLengthDict = new HashMap<Integer,Integer>();
	private HashMap<Integer,HashMap<Integer,Integer>> inverseDocumentTermsDictionary = new HashMap<Integer,HashMap<Integer,Integer>>();
	String pattern ;
	String dirNameTerm;
	String dirNameCategory;
	String dirNameAuth ;
	String dirNamePlace;
	String dirName;
	public IndexWriter(String indexDir) {
		//TODO : YOU MUST IMPLEMENT THIS
		this.mainIndexDir = indexDir;
		for(int i = 0;i<BUCKETS;i++){
			authorIndex[i] = new HashMap<Integer, HashMap<Integer,Integer>>();
			categoryIndex[i] = new HashMap<Integer, HashMap<Integer,Integer>>();
			placeIndex[i] = new HashMap<Integer, HashMap<Integer,Integer>>();
			termIndex[i] = new HashMap<Integer, HashMap<Integer,Integer>>();
		}
		pattern = System.getProperty("file.separator");
		dirNameTerm = mainIndexDir+pattern+"term";
		dirNameCategory = mainIndexDir+pattern+"category";
		dirNameAuth = mainIndexDir+pattern+"author";
		dirNamePlace = mainIndexDir+pattern+"place";
		dirName = mainIndexDir+pattern+"document";
		createDirectoryStructure();
		
	}
	
	
	/**
	 * Method to add the given Document to the index
	 * This method should take care of reading the filed values, passing
	 * them through corresponding analyzers and then indexing the results
	 * for each indexable field within the document. 
	 * @param d : The Document to be added
	 * @throws IndexerException : In case any error occurs
	 */
	@SuppressWarnings("unchecked")
	public void addDocument(Document d) throws IndexerException {
		//TODO : YOU MUST IMPLEMENT THIS
		TokenStream authorStream = getStream(FieldNames.AUTHOR,d,null);
		TokenStream authorOrgStream = getStream(FieldNames.AUTHORORG,d,null);
		TokenStream categoryStream = getStream(FieldNames.CATEGORY,d,null);
		TokenStream contentStream = getStream(FieldNames.CONTENT,d);
		TokenStream newsDateStream = getStream(FieldNames.NEWSDATE,d,null);
		TokenStream placeStream = getStream(FieldNames.PLACE,d,null);
		TokenStream titleStream = getStream(FieldNames.TITLE,d);
		AnalyzerFactory analyzerFactory = AnalyzerFactory.getInstance();
		
		if(authorStream!=null){
			authorStream = analyzerFactory.getAnalyzerForField(FieldNames.AUTHOR, authorStream).getStream();//This is call to chain filter
		}
		if(authorOrgStream!=null)
		{
			authorOrgStream = analyzerFactory.getAnalyzerForField(FieldNames.AUTHORORG, authorOrgStream).getStream();//This is call to chain filter
		}
		if(newsDateStream!=null)
		{
			newsDateStream = analyzerFactory.getAnalyzerForField(FieldNames.NEWSDATE, newsDateStream).getStream();//This is call to chain filter
		}
		if(titleStream!=null){
			titleStream = analyzerFactory.getAnalyzerForField(FieldNames.TITLE, titleStream).getStream();//This is call to chain filter
			
		}
		if(placeStream!=null)
		{
			placeStream = analyzerFactory.getAnalyzerForField(FieldNames.PLACE, placeStream).getStream();//This is call to chain filter
		}
		if(categoryStream!=null)
		{
			categoryStream = analyzerFactory.getAnalyzerForField(FieldNames.CATEGORY, categoryStream).getStream();//This is call to chain filter
		}
		if(contentStream!=null){
			contentStream = analyzerFactory.getAnalyzerForField(FieldNames.CONTENT, contentStream).getStream();//This is call to chain filter									
		}

		docLengthDict.put(documentI,docLength);
		if(inverseDocumentTermsDictionary.get(documentI)==null)
		{
			inverseDocumentTermsDictionary.put(documentI, new HashMap<Integer,Integer>());
		}
		docTotalLength = docTotalLength + docLength;
		docLength = 0;
		writeDictionary(authorIndex,authorDict,authorStream,authorI,IndexType.AUTHOR);
		writeDictionary(placeIndex,placeDict,placeStream,placeI,IndexType.PLACE);		
		writeDictionary(termIndex,termDict,contentStream,termI,IndexType.TERM);
		writeDictionary(termIndex,termDict,authorOrgStream,termI,IndexType.TERM);
		writeDictionary(termIndex,termDict,newsDateStream,termI,IndexType.TERM);
		writeDictionary(termIndex,termDict,titleStream,termI,IndexType.TERM);
		writeDictionary(categoryIndex,categoryDict,categoryStream,categoryI,IndexType.CATEGORY);		
		
		writeDocumentDictionary(d);	
		
		if(termIndex[0].size()>MAX_STREAM_LIST_SIZE){			
			
			writeIndexes();
			writeCountTerm++;
		
		}

	}
	
	private void createDirectoryStructure(){
		makeDirectory(dirName);
		makeDirectory(dirNameAuth);
		makeDirectory(dirNamePlace);
		makeDirectory(dirNameCategory);
		makeDirectory(dirNameTerm);
		makeDirectory(dirNameAuth+pattern+"temp");
		makeDirectory(dirNamePlace+pattern+"temp");
		makeDirectory(dirNameCategory+pattern+"temp");
		makeDirectory(dirNameTerm+pattern+"temp");
	}
	
	public void writeDictToFile(HashMap<String, Integer> authorDict2,String s){
	    //write to file : "fileone"
		try{
		    File fileTwo=new File(s);
		    fileTwo.createNewFile();
		    FileOutputStream fos=new FileOutputStream(fileTwo);
		        PrintWriter pw=new PrintWriter(fos);
		        Map<String, Integer> map = new TreeMap<String, Integer>(authorDict2);
		        for(Map.Entry<String,Integer> m :map.entrySet()){
		            pw.println(m.getKey()+"="+m.getValue());
		        }
		        pw.flush();
		        pw.close();
		        fos.close();
		    }catch(Exception e){}

	  }
	
	public void writeTermIndexToFile(HashMap<Integer, HashMap<Integer,Integer>> termIndex2,String s){
	    //write to file : "fileone"		 
		try{			
		    File fileTwo=new File(s);
		    fileTwo.createNewFile();
		    FileOutputStream fos=new FileOutputStream(fileTwo);
		        PrintWriter pw=new PrintWriter(fos);
		        Map<Integer,HashMap<Integer,Integer>> map = new TreeMap<Integer, HashMap<Integer,Integer>>(termIndex2);
		        
		        for(Entry<Integer, HashMap<Integer,Integer>> m :map.entrySet()){
		        	
		        	 Map<Integer,Integer> child = new TreeMap<Integer,Integer>(m.getValue());
		        	pw.println(m.getKey()+"="+child);
		        }
		        pw.flush();
		        pw.close();
		        fos.close();
		    }catch(Exception e){
		    	e.printStackTrace();
		    }

	  }
	
	
	private TokenStream getStream(FieldNames f, Document d){		//Polymorphism
		Tokenizer tkizer = new Tokenizer();
		String []s =  d.getField(f);
		TokenStream stream = null;
		if(s!=null){
			stream = new TokenStream();			
			for(int i=0;i<s.length;i++){
				
				if(s[i]!=null&&!s[i].isEmpty()){
					try {															
							stream.append(tkizer.consume(s[i]));																						
					} catch (TokenizerException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace(); Akash Deshpande. There is some issue here. As when the tokenizer consume is called too fast it tends to throws a error here.
					}		
				}	
			}	
			stream.reset();
		}
		return stream;				
	}
	private TokenStream getStream(FieldNames f, Document d, String delimiter){		
		Tokenizer tkizer = new Tokenizer(delimiter);
		String []s =  d.getField(f);
		TokenStream stream = new TokenStream();
		
		if(s!=null){
			for(int i=0;i<s.length;i++){
				
				if(s[i]!=null&&!s[i].isEmpty()){
					try {							
						stream.append(tkizer.consume(s[i]));						
					} catch (TokenizerException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();//Akash Deshpande error. 
					}	
				}		
			}	
		}
		stream.reset();
		return stream;				
	}
	
	private void  writeDocumentDictionary(Document d) throws IndexerException{
		String id = d.getField(FieldNames.FILEID)[0];		
		if(documentDict.get(id)==null)
		{
			documentDict.put(id,documentI);
			documentI++;
		}
	}
	private void  writeDictionary(HashMap<Integer, HashMap<Integer,Integer>> [] index,
			CaseInsensitiveMap dictionary, TokenStream stream, Integer count,IndexType type) throws IndexerException{		
		if(stream!=null){			
			stream.reset();
			String s ;
			String [] temp;
			int current, iterator;
			
			while(stream.hasNext()){
				
				s = stream.next().toString();
				if(!s.isEmpty()){
				iterator = getTermBucket(s);
				temp = s.split(" AND ");
				if(temp.length==1){
					temp = temp[0].split(" and ");
				}
				for(String t:temp)
					{
					
					t = t.trim();
					if(dictionary.get(t)==null){
						dictionary.put(t, count);					
						count++;							
					}
					docLength++;
					current = dictionary.get(t);	
					int i = 1;
					HashMap<Integer,Integer> terms = index[iterator].get(current);
					
					if(terms == null){					
						terms = new  HashMap<Integer,Integer>();					
						terms.put(documentI,i);					
					}
					else{
						
						if(terms.get(documentI)==null){
							i = 1;
						}
						else{
						i = terms.get(documentI);
						i+=1;}
						terms.put(documentI, i);
					}		
					if(inverseDocumentTermsDictionary.get(documentI)!=null)
					{
						HashMap<Integer,Integer> h= inverseDocumentTermsDictionary.get(documentI);
						
						h.put(current,i);
						inverseDocumentTermsDictionary.put(documentI, h);						
					}
					index[iterator].put(current,terms);
					
					}
			}
				
			}
			switch(type){
			case TERM:
				termDict = dictionary;
				termIndex = index;
				termI = count;
				break;
			case CATEGORY:
				categoryDict = dictionary;
				categoryIndex = index;
				categoryI = count;
				break;
			case AUTHOR:
				authorDict = dictionary;
				authorIndex = index;
				authorI = count;
				break;
			case PLACE:
				placeDict = dictionary;
				placeIndex = index;
				placeI = count;
				break;
			
			}
			stream.reset();
		}
	}

	private Integer getTermBucket(String s) throws IndexerException{
		if(!s.isEmpty()&&s!=null){

		char t = s.toUpperCase().charAt(0);		
		int value = Character.getNumericValue(t)-10;
		if(value>=BUCKETS||value<0)
			value = BUCKETS-1;
		return value;
		}
		return BUCKETS-1;
	}
	private Character getCharacterBucket(int i) throws IndexerException{
		i = i+10;						
		char value = Character.toUpperCase(Character.forDigit(i,Character.MAX_RADIX));		
		return value;
	}
	private Boolean makeDirectory(String s){
		File theDir = new File(s);
		boolean result = false;
		  // if the directory does not exist, create it
		  if (!theDir.exists()) {		    
		    try{
		        theDir.mkdir();
		        result = true;
		     } catch(SecurityException se){
		        //handle it
		    	// se.printStackTrace();
		     }        		     
		  }
		  else
			  result=true;  
		return result;
		
	}		
	private void writeDictionary() throws IndexerException{
			writeDictToFile(documentDict,mainIndexDir+pattern+"document"+pattern+"documentDict.txt");				
			writeDictToFile(authorDict,dirNameAuth+pattern+"authorDict.txt");			
			writeDictToFile(placeDict,dirNamePlace+pattern+"placeDict.txt");			
			writeDictToFile(categoryDict,dirNameCategory+pattern+"categoryDict.txt");			
			writeDictToFile(termDict,dirNameTerm+pattern+"termDict.txt");			
	}
	
	@SuppressWarnings("unchecked")
	private void writeIndexes() throws IndexerException{				
		String count = Integer.toString(writeCountTerm);
		try{
			for(int i=0;i<BUCKETS;i++){			
					writeTermIndexToFile(authorIndex[i],dirNameAuth+pattern+"temp"+pattern+"authorBucket"+i+"Index"+count+".txt");
					writeTermIndexToFile(placeIndex[i],dirNamePlace+pattern+"temp"+pattern+"placeBucket"+i+"Index"+count+".txt");
					writeTermIndexToFile(categoryIndex[i],dirNameCategory+pattern+"temp"+pattern+"categoryBucket"+i+"Index"+count+".txt");			
					writeTermIndexToFile(termIndex[i],dirNameTerm+pattern+"temp"+pattern+"termBucket"+i+"Index"+count+".txt");
					placeIndex[i].clear();
					categoryIndex[i].clear();
					authorIndex[i].clear();
					termIndex[i].clear();
				}
		}
		catch( Exception e){
			throw new IndexerException();
		}
	}
	/**
	 * Method that indicates that all open resources must be closed
	 * and cleaned and that the entire indexing operation has been completed.
	 * @throws IndexerException : In case any error occurs
	 */
	@SuppressWarnings("unchecked")
	private void mergeIndexes(final String name) throws IndexerException{
		String path = mainIndexDir+pattern+name+pattern+"temp";
		File indexDirectory = new File(path);
		HashMap<Integer, HashMap<Integer,Integer>> mapInFile=new HashMap<Integer, HashMap<Integer,Integer>>();
		HashMap<Integer, HashMap<Integer,Integer>> temp=new HashMap<Integer, HashMap<Integer,Integer>>();
		HashMap<Integer,Integer> tempChild,mapInfileChild,merged = new HashMap<Integer,Integer>();
		
		for( int i = 0;i<BUCKETS-1;i++){
				final int j = i;				
				File[] fileList = indexDirectory.listFiles(new FilenameFilter() {
				    public boolean accept(File directory, String fileName) {				    	
				        return fileName.contains(name+"Bucket"+Integer.toString(j)+"Index"); 
				    }				
				});				
				
				for(int k =0;k<fileList.length;k++){
					temp = loadTermIndex(fileList[k]);
					for(int m:temp.keySet()){
						if(mapInFile.get(m)==null){																					
							mapInFile.put(m,temp.get(m));							
						}
						else{
								tempChild = temp.get(m);
								
								mapInfileChild = mapInFile.get(m);																
								for (Integer x : tempChild.keySet()) {
									   Integer y = mapInfileChild.get(x);
									   
									   
									   if (y == null) {
									      merged.put(x, tempChild.get(x));
									      
									   } else {
									      merged.put(x, tempChild.get(x)+y);
									   }
									   
									}
								for (Integer x : mapInfileChild.keySet()) {
									   if (merged.get(x) == null) {
									      merged.put(x, mapInfileChild.get(x));
									   }
									}
								
								if(!merged.isEmpty())
								{
									mapInFile.put(m, (HashMap<Integer, Integer>) merged.clone());									
									merged.clear();																		
								}	
								
							}
						}
					
					}		
																		
				
				writeTermIndexToFile(mapInFile,mainIndexDir+pattern+name+pattern+name+"BucketIndex"+getCharacterBucket(i)+".txt");
				mapInFile.clear();
				}
	}
	private HashMap<Integer, HashMap<Integer,Integer>> loadTermIndex(File toRead) throws IndexerException{
		
		HashMap<Integer, HashMap<Integer,Integer>> mapInFile=new HashMap<Integer, HashMap<Integer,Integer>>();
		 try{
		        
		        FileInputStream fis=new FileInputStream(toRead);
		        Scanner sc=new Scanner(fis);		      
		        //read data from file line by line:
		        String currentLine;
		        
		        while(sc.hasNextLine()){
		        	HashMap<Integer, Integer> integerMap = new HashMap<Integer, Integer>();
		            currentLine=sc.nextLine();			            		            
		           
		            StringTokenizer st= new StringTokenizer(currentLine,"=",false);
		            StringTokenizer sn ;
		            String [] numberMap = currentLine.split("\\{")[1].split("\\}")[0].split(",");		            	            		           
		            for(String num:numberMap)
		            {		    		            	
		            	sn = new StringTokenizer(num.trim(),"=",false);			            	
		            	integerMap.put(Integer.parseInt(sn.nextToken()), Integer.parseInt(sn.nextToken()));		            			            	
		            }		 
		            
		            mapInFile.put(Integer.parseInt(st.nextToken()),integerMap);		           
		        }
		        
		        fis.close();		        
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		 
		return mapInFile;
		
	}
	private void callMergeDeleteIndexes() throws IndexerException{
		mergeIndexes("term");
		mergeIndexes("category");
		mergeIndexes("author");
		mergeIndexes("place");
		deleteTempIndexes("term");
		deleteTempIndexes("category");
		deleteTempIndexes("author");
		deleteTempIndexes("place");
	}
	
	
	private void writeDocumentLenthToFile(){
		String s = mainIndexDir+pattern+"document"+pattern+"documentLengthDict.txt";
		String s1 = mainIndexDir+pattern+"document"+pattern+"documentAvgLength.txt";
		 //write to file : "fileone"
		try{
		    File fileTwo=new File(s);
		   
		    fileTwo.createNewFile();
		   
		    FileOutputStream fos=new FileOutputStream(fileTwo);
		        PrintWriter pw=new PrintWriter(fos);
		        Map<Integer, Integer> map = new TreeMap<Integer, Integer>(docLengthDict);
		        for(Map.Entry<Integer,Integer> m :map.entrySet()){
		            pw.println(m.getKey()+"="+m.getValue());
		        }
		    
		        pw.flush();
		        pw.close();
		        fos.close();
		        PrintWriter writer = new PrintWriter(s1, "UTF-8");
		        writer.println(docTotalLength/documentI);		    
		        writer.close();
		    }catch(Exception e){}

	  }
	
	
	private void deleteTempIndexes(String name) throws IndexerException{
		String path = mainIndexDir+pattern+name+pattern+"temp";
		File f = new File(path);
		delete(f);
	}
	void delete(File f) throws IndexerException {
		try{  
			if (f.isDirectory()) {
			    for (File c : f.listFiles())
			      delete(c);
			  }
			  f.delete();
			}
		catch(Exception e){
			throw new IndexerException();
		}
	}
	public void close() throws IndexerException {
		//TODO	
		
		writeDocumentLenthToFile();
		writeInverseDocumentTermDictionary();
		writeDictionary();
		writeIndexes();
		callMergeDeleteIndexes();
	}


	private void writeInverseDocumentTermDictionary() {
		// TODO Auto-generated method stub
		
		String s1 = mainIndexDir+pattern+"document"+pattern+"inverseDocumentTermDict.txt";
		 //write to file : "fileone"
		try{
		    File fileTwo=new File(s1);
		   
		    fileTwo.createNewFile();
		   
		    FileOutputStream fos=new FileOutputStream(fileTwo);
		        PrintWriter pw=new PrintWriter(fos);
		        Map<Integer,  HashMap<Integer,Integer>> map = new TreeMap<Integer, HashMap<Integer,Integer>>(inverseDocumentTermsDictionary);
		        for(Map.Entry<Integer, HashMap<Integer,Integer>> m :map.entrySet()){
		        	 Map<Integer,Integer> child = new TreeMap<Integer,Integer>(m.getValue());
		            pw.println(m.getKey()+"="+child);
		        }
		    
		        pw.flush();
		        pw.close();
		        fos.close();
		        
		    }catch(Exception e){}
	}
}
