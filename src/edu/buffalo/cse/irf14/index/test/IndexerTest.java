/**
 * 
 *//*
package edu.buffalo.cse.irf14.index.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.buffalo.cse.irf14.analysis.Analyzer;
import edu.buffalo.cse.irf14.analysis.AnalyzerFactory;
import edu.buffalo.cse.irf14.analysis.TokenStream;
import edu.buffalo.cse.irf14.analysis.Tokenizer;
import edu.buffalo.cse.irf14.analysis.TokenizerException;
import edu.buffalo.cse.irf14.document.Document;
import edu.buffalo.cse.irf14.document.FieldNames;
import edu.buffalo.cse.irf14.index.IndexReader;
import edu.buffalo.cse.irf14.index.IndexType;
import edu.buffalo.cse.irf14.index.IndexWriter;
import edu.buffalo.cse.irf14.index.IndexerException;
import edu.buffalo.cse.irf14.query.QueryType;

*//**
 * @author nikhillo
 *
 *//*
public class IndexerTest {
	private IndexReader reader;
	
	@BeforeClass
	public final static void setupIndex() throws IndexerException {
		String[] strs = {"new home sales top sales forecasts", "home sales rise in july", 
				"increase in home sales in july", "july new home sales rise"};
		int len = strs.length;
		Document d;
		String dir = "E:\\Studies PDF Sem 1\\CSE - 535 Information Retrieval\\Project\\Indexes";
		IndexWriter writer = new IndexWriter(dir); //set this beforehand
		for (int i = 0; i < len; i++) {
			d = new Document();
			d.setField(FieldNames.FILEID, "0000"+(i+1));
			d.setField(FieldNames.CONTENT, strs[i]);
			writer.addDocument(d);
		}
		
		writer.close();
	}

	@Before
	public final void before() {
		reader = new IndexReader("E:\\Studies PDF Sem 1\\CSE - 535 Information Retrieval\\Project\\Indexes", IndexType.TERM);
	}

	private static String getAnalyzedTerm(String string) {
		Tokenizer tknizer = new Tokenizer();
		AnalyzerFactory fact = AnalyzerFactory.getInstance();
		try {
			TokenStream stream = tknizer.consume(string);
			Analyzer analyzer = fact.getAnalyzerForField(FieldNames.CONTENT, stream);
			
			while (analyzer.increment()) {
				
			}
			stream = analyzer.getStream();
			stream.reset();
			return stream.next().toString();
		} catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	*//**
	 * Test method for {@link edu.buffalo.cse.irf14.index.IndexReader#getTopK(int)}.
	 *//*
	@Test
	public final void testGetTopK() {
		//positive cases
		List<String> topK = null;
		String[] vals = {"sales", "home", "july"};
		
		for (int i = 0; i < 3; i++) {
			vals[i] = getAnalyzedTerm(vals[i]);
		}
		
		for (int i = 0; i < 3; i++) {
			topK = reader.getTopK(i + 1);
			assertNotNull(topK);
			assertEquals(i + 1, topK.size(), 0);
			
			for (int j = 0; j <=i; j++) {
				assertEquals(vals[j], topK.get(j));
			}
		}
		
		//negative case
		assertNull(reader.getTopK(-1));
		assertNull(reader.getTopK(0));
	}

	*//**
	 * Test method for {@link edu.buffalo.cse.irf14.index.IndexReader#query(java.lang.String[])}.
	 *//*
	@Test
	public final void testAndQuery() {
		String[] queryTerms = {"sales", "forecasts"};
		String[] docs = {"0001"};
		int len = queryTerms.length;
		
		
		for (int i = 0; i <len; i++) {
			queryTerms[i] = getAnalyzedTerm(queryTerms[i]);
		}
		
		HashSet<String> t= reader.query(queryTerms[0], queryTerms[1],QueryType.AND);
		assertEquals(1, t.size(), 0);			
		for(int i=0;i<t.size();i++){
			if(t.contains(docs[i])){
				assertEquals(1,1,1);}
			else{
				assertEquals(1,0,1);}			
		}	
	}

	
	
	*//**
	 * Test method for {@link edu.buffalo.cse.irf14.index.IndexReader#query(java.lang.String[])}.
	 *//*
	@Test
	public final void testOrQuery() {
		String[] queryTerms = {"forecasts", "rise"};
		String[] docs = {"0001","0002","0004"};
		int len = queryTerms.length;
		
		
		for (int i = 0; i <len; i++) {
			queryTerms[i] = getAnalyzedTerm(queryTerms[i]);
		}
		
		HashSet<String> t= reader.query(queryTerms[0], queryTerms[1],QueryType.OR);
		assertEquals(3, t.size(), 0);			
		for(int i=0;i<t.size();i++){
			if(t.contains(docs[i])){
				assertEquals(1,1,1);}
			else{
				assertEquals(1,0,1);}			
		}	
	}
	
	@Test
	public final void testTermFrequency() {
		String[] queryTerms = {"sales"};		
		int len = queryTerms.length;		
		for (int i = 0; i <len; i++) {
			queryTerms[i] = getAnalyzedTerm(queryTerms[i]);
		}
		
		Integer t= reader.getTermFrequency(queryTerms[0],0);
		assertNotNull(t);
		assertEquals(2, t, 0);			
		
		t= reader.getTermFrequency(queryTerms[0],1);
		assertNotNull(t);
		assertEquals(1, t, 0);	
	}
	
	
	
	
	@Test
	public final void testNotQuery() {
		String[] queryTerms = {"sales", "rise"};
		String[] docs = {"0001","0003"};
		int len = queryTerms.length;
		
		
		for (int i = 0; i <len; i++) {
			queryTerms[i] = getAnalyzedTerm(queryTerms[i]);
		}
		
		HashSet<String> t= reader.query(queryTerms[0], queryTerms[1],QueryType.NOT);
		assertNotNull(t);
		assertEquals(2, t.size(), 0);			
		for(int i=0;i<t.size();i++){
			if(t.contains(docs[i])){
				assertEquals(1,1,1);}
			else{
				assertEquals(1,0,1);}			
		}	
	}
	
	
	
	
	

}
*/