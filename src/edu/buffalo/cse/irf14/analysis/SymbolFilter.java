package edu.buffalo.cse.irf14.analysis;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SymbolFilter extends TokenFilter{

	public SymbolFilter(TokenStream stream) {
		super(stream);
		// TODO Auto-generated constructor stub		
		super.mainStream.reset();		
		String s = "";		
		Token t = null;
		String[] stringArray = null;		
		int i = 0;
		try {
			while(increment()){			
				t = super.mainStream.getCurrent();
				s = s + " " +t.getTermText();
				
			}	
		} catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			stringArray = apostrophe(s.trim());
		} catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.mainStream.removeAll();
		for(i=0;i<stringArray.length;i++){
			if(stringArray[i]!=null&&!stringArray[i].isEmpty()){
				t = new Token();
				t.setTermText(stringArray[i]);
				
				super.mainStream.addToken(t);
				
			}
		}	
		
		super.mainStream.reset();		
	}

	@Override
	public boolean increment() throws TokenizerException {		
		// TODO Auto-generated method stub		
		if(super.mainStream.hasNext()){
			super.mainStream.next();
			return true;
		}
		return false;
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		
		return super.mainStream;
	}

	//apostrophe 
	//punctuation
	//hyphen
	private String[] apostrophe(String current) throws TokenizerException{	

		String[] stringArray = null;
		String temp = "";
		int i =0;
		if(current.contains("'")){

			if(current.matches(".*\\'([^\\']*)\\'.*"))
			{
				Pattern p = Pattern.compile("\\'([^\\']*)\\'");
				Matcher m = p.matcher(current);
				while (m.find()) {
					temp = m.group(1);
				}


				current = current.replace("'" + temp + "'", temp);
			}
			if(current.contains("'ve"))
			{				
				current = current.replaceAll("'ve", "__have");
			}			
			if(current.contains("'re"))
			{
				current = current.replaceAll("'re", "__are");
			}			
			if(current.contains("'em"))
			{
				current = current.replaceAll("'em", "them");
			}
			if(current.contains("'ll"))
			{
				current = current.replaceAll("'ll", "__will");
			}
			if(current.contains("'d"))
			{
				current = current.replaceAll("'d", "__would");
			}
			if(current.contains("'s"))
			{
				current = current.replaceAll("'s", "");
			}
			if(current.contains("s'"))
			{
				current = current.replaceAll("s'", "s");
			}			
			if(current.contains("n't"))
			{
				current = current.replaceAll("won't", "will__not");
				current = current.replaceAll("isn't", "is__not");
				current = current.replaceAll("don't", "do__not");
				current = current.replaceAll("shan't", "shall__not");
				current = current.replaceAll("doesn't", "does__not");
				current = current.replaceAll("aren't", "are__not");
				current = current.replaceAll("couldn't", "could__not");
				current = current.replaceAll("wouldn't", "would__not");
				//there are a couple of cases
			}
			if(current.contains("'m"))
			{
				current = current.replaceAll("'m", "__am");
			}
			if(current.contains("''")){
				current = current.replace("''", "");
			}
			if(current.contains("'(")){
				current = current.replace("'(", "(");
			}
			if(current.contains("'/")){
				current = current.replace("'/", "/");
			}if(current.matches(".*\\'([^\\']*)\\'.*"))
			{
				
			}
		}	

		if(current.contains(".") || current.contains("?") || current.contains("!")){
			char stringArray1[] = current.toCharArray();						
			//To remove punctuation from the end of a sentence
			while(true){
				if(current.matches(".*\\.\\.+.*")){
					current = current.replaceAll("\\.\\.+", "");
				}
				if(current.matches(".*\\.$")){
					stringArray1 = current.toCharArray();
					stringArray1[current.length()-1] = ' ';
					if(current.length()>1)
					i = current.length() - 2 ;
					if(stringArray1[i]=='.')
					while(stringArray1[i]=='.' && i>=0){
						stringArray1[i]=' ';
						i=i-1;
					}
					current = new String(stringArray1);
					current = current.trim();
				}
				else if(current.matches(".*\\!$")){
					stringArray1 = current.toCharArray();
					stringArray1[current.length()-1] = ' ';
					current = new String(stringArray1);
					current = current.trim();  
				}
				else if(current.matches(".*\\?$")){
					stringArray1 = current.toCharArray();
					stringArray1[current.length()-1] = ' ';
					current = new String(stringArray1);
					current = current.trim();
				}
				else if(current.matches(".*\\.\\s.*")){
					//test2 = test.toCharArray();
					current = current.replaceAll("\\.\\s", " ");
					//test = new String(test2);
					current = current.trim();
				}
				else if(current.matches(".*\\?\\s.*")){
					//test2 = test.toCharArray();
					current = current.replaceAll("\\?\\s", " ");
					//test = new String(test2);
					current = current.trim();
				}
				else if(current.matches(".*\\!\\s.*")){
					//test2 = test.toCharArray();
					current = current.replaceAll("\\!\\s", " ");
					//test = new String(test2);
					current = current.trim();
				}
				else{
					//now all token from end are removed
					break;
				}
			}//end of sentence ends
			//for ip address
			if(current.matches("(\\d+\\.\\d+\\.*)+")){
				//keep this 
			}
		}

		//hyphen
		if(current.contains("-")){
			//char stringArray1[] = current.toCharArray();
			//int i = 0;
			//i=current.indexOf('-');

			if(current.matches(".*\\s*--+.*")){
				current = current.replaceAll("-", "");				
				current  = current.trim();
				
			}
		
			else if(current.matches("[A-Za-z]+-[A-Za-z]+")){
				current = current.replaceFirst("-" , "__");
				//caution there may be tokens containing more than one hyphen
			}
			else if(current.matches("[A-Za-z]+\\s+-+\\s+[A-Za-z]+")){
				current = current.replaceFirst("-" , " ");
			}
			
		}




		current = current.trim();
		stringArray = current.split(" ");
		
		for(i=0;i<stringArray.length;i++)
		{
			stringArray[i] = stringArray[i].replaceAll("__", " ");
		}
		return stringArray;
	}









	String[] hyphen(String current){				

		String[] stringString = null;		
		if(current.contains("-")){
			char stringArray[] = current.toCharArray();
			int i = 0;
			i=current.indexOf('-');


			//no. of consecutive hyphens like c----- or -----c

			if(current.matches(".*\\b--+[a-zA-z]+\\b.*") || current.matches(".*\\b[a-zA-z]+--+\\b.*")){
				
				current = current.replaceAll("-", "");				
				current  = current.trim();

			}
			
			if(current.matches(".*\\d.*")){
				//contains a digit retain full token
			}

			/*if(Character.isDigit(stringArray[i-1]) || Character.isDigit(stringArray[i+1])){
				//if there is a number before or after the hyphen keep it as it is

			}*/

			else if(current.matches("[A-Za-z]+-[A-Za-z]+")){
				current = current.replaceFirst("-" , " ");
				//caution there may be tokens containing more than one hyphen
			}

			else if(Character.isWhitespace(stringArray[i-1]) && Character.isWhitespace(stringArray[i+1])){
				current = current.replace("-", " ");

				//make two tokens
			}

			else if(current.length() == 1){
				//this means that this token only contains a hyphen
				//remove this token
			}



		}

		stringString = current.split(" ");

		return stringString;

	}//hypen() ends

	String[] punctuation(String current){								
		String[] stringString = null;
		if(current.contains(".") || current.contains("?") || current.contains("!")){
			char stringArray[] = current.toCharArray();						
			//To remove punctuation from the end of a sentence
			while(true){
				if(current.matches(".*\\.$")){
					stringArray = current.toCharArray();
					stringArray[current.length()-1] = ' ';
					current = new String(stringArray);
					current = current.trim();
				}
				else if(current.matches(".*\\!$")){
					stringArray = current.toCharArray();
					stringArray[current.length()-1] = ' ';
					current = new String(stringArray);
					current = current.trim();  
				}
				else if(current.matches(".*\\?$")){
					stringArray = current.toCharArray();
					stringArray[current.length()-1] = ' ';
					current = new String(stringArray);
					current = current.trim();
				}
				else if(current.matches(".*\\.\\s.*")){
					//test2 = test.toCharArray();
					current = current.replaceAll("\\.\\s", " ");
					//test = new String(test2);
					current = current.trim();
				}
				else{
					//now all token from end are removed
					break;
				}
			}//end of sentence ends
			//for ip address
			if(current.matches("(\\d+\\.\\d+\\.*)+")){
				//keep this 
			}
		}


		stringString = current.split(" ");
		return stringString;
	}



}
