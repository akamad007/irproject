package edu.buffalo.cse.irf14.analysis;


public class StopwordFilter extends TokenFilter {
	//String stopWordString = "able,about,across,after,all,almost,also,,among,,and,any,are,because,been,but,can,cannot,could,dear,did,does,either,else,ever,every,for,from,get,got,had,has,have,her,hers,him,his,how,however,into,its,just,least,let,like,likely,may,,might,most,must,my,neither,nor,not,,off,often,only,other,our,own,rather,said,say,says,she,should,since,so,some,than,that,the,their,them,then,there,these,they,this,tis,,too,twas,wants,was,,were,what,when,where,which,while,who,whom,why,will,with,would,yet,you,your";
	final String[] stopWords1 ={"a","i"};
	final String[] stopWords2 ={"am","an","as","at","be","by","do","he","no","on","or","of","to","we","me","if","in","is","it","us","my","so"};
	final String[] stopWords3 ={"all","and","any","are","but","can","did","for","get","got","had","has","her","him","his","how","its","let","may","neither","nor","not","off","our","own","she","the","tis","too,was","who","why","yet","you"};
	final String[] stopWords4 = {"able","been","does","else","ever","from","have","hers","into","just","like","most","must","only","said","says","some","than","that","them","then","they","this","twas","wants","were","what","when","whom","will","with","your"};
	final String[] stopWords5 = {"about","after","among","could","every","least","might","often","other","since","their","these","where","which","while","would"};
	final String[] stopWords6 = {"across","almost","cannot","either","likely","rather","should,"};
	final String[] stopWords7 = {"because","however","neither"};
	public StopwordFilter(TokenStream stream) {
	
		super(stream);
		// TODO Auto-generated constructor stub		
		super.mainStream.reset();
		String s = "";
		String []stringArray;
		String st = null;
				try {
					while(increment()){			
						Token t = super.mainStream.getCurrent();						
						st = this.filter(t.getTermText());
						if(st!=null){
							s=s+" "+st;
						}
					}
				} catch (TokenizerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
		super.mainStream.removeAll();
		stringArray = s.split(" ");
		for(int i =0;i<stringArray.length;i++)
		{
			Token t = new Token();
			t.setTermText(stringArray[i]);
			super.mainStream.addToken(t);
		}
		super.mainStream.reset();		
	}

	@Override
	public boolean increment() throws TokenizerException {		
		// TODO Auto-generated method stub		
		if(super.mainStream.hasNext()){
			super.mainStream.next();
			return true;
		}
		return false;
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		return super.mainStream;
	}
	
	private String filter(String tokenString) throws TokenizerException {			
		int i;
		if(tokenString.length()<8){
		
			switch(tokenString.length()){
			case 1:
				for(i=0;i<stopWords1.length;i++)
				{
					if(tokenString.equals(stopWords1[i])||tokenString.equals(stopWords1[i].toUpperCase()))
					{
						return null;
					}
				}
				break;
			case 2:
				for(i=0;i<stopWords2.length;i++)
				{
					if(tokenString.equals(stopWords2[i])||tokenString.equals(stopWords2[i].toUpperCase()))
					{
						return null;
					}
				}
				
				break;	
				
			case 3:
				for(i=0;i<stopWords3.length;i++)
				{
					if(tokenString.equals(stopWords3[i])||tokenString.equals(stopWords3[i].toUpperCase()))
					{
						return null;
					}
				}
				break;	
			case 4:
				for(i=0;i<stopWords4.length;i++)
				{
					if(tokenString.equals(stopWords4[i])||tokenString.equals(stopWords4[i].toUpperCase()))
					{
						return null;
					}
				}
				break;
			case 5:
				for(i=0;i<stopWords5.length;i++)
				{
					if(tokenString.equals(stopWords5[i])||tokenString.equals(stopWords5[i].toUpperCase()))
					{
						return null;
					}
				}
				break;
			case 6:
				for(i=0;i<stopWords6.length;i++)
				{
					if(tokenString.equals(stopWords6[i])||tokenString.equals(stopWords6[i].toUpperCase()))
					{
						return null;
					}
				}
				break;
			case 7:
				for(i=0;i<stopWords7.length;i++)
				{
					if(tokenString.equals(stopWords7[i])||tokenString.equals(stopWords7[i].toUpperCase()))
					{
						return null;
					}
				}
				break;
			
			default:
				return null;
			}
			
		}	
		return tokenString;
		}

}
