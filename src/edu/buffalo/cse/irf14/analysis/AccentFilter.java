package edu.buffalo.cse.irf14.analysis;

import java.text.Normalizer;
import java.text.ParseException;
import java.util.regex.Pattern;

public class AccentFilter extends TokenFilter{

	public AccentFilter(TokenStream stream) {
		super(stream);
		// TODO Auto-generated constructor stub		
		super.mainStream.reset();
		String s = "";		
		Token t = null;
		String[] stringArray;
		try {
			while(increment()){			
				t = super.mainStream.getCurrent();
				s = s + " " +t.getTermText();
			}	
		} catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			stringArray = this.filter(s.trim());
			super.mainStream.removeAll();
			for(int i=0;i<stringArray.length;i++){
				if(stringArray[i]!=null){
					t = new Token();
					t.setTermText(stringArray[i]);
					super.mainStream.addToken(t);
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.mainStream.reset();		
	}

	@Override
	public boolean increment() throws TokenizerException {		
		// TODO Auto-generated method stub		
		if(super.mainStream.hasNext()){
			super.mainStream.next();
			return true;
		}
		return false;
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		return super.mainStream;
	}

	private String[] filter(String tokenString) throws ParseException{			
		String[] stringArray = null;
	    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		tokenString = Normalizer.normalize(tokenString, Normalizer.Form.NFD);		
		tokenString = pattern.matcher(tokenString).replaceAll("");
		tokenString.trim();
		stringArray = tokenString.split(" ");
		return stringArray;
	}

}
