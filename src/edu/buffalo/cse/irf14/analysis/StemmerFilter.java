package edu.buffalo.cse.irf14.analysis;

import java.text.ParseException;


public class StemmerFilter extends TokenFilter {

	public StemmerFilter(TokenStream stream) {
		super(stream);
		// TODO Auto-generated constructor stub		
		super.mainStream.reset();
		String s = "";		
		Token t = null;
		String[] stringArray;
		try {
			while(increment()){			
				t = super.mainStream.getCurrent();
				s = s + " " +t.getTermText();
				
			}	
			
		} catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {			
			stringArray = this.filter(s.trim());
			
			super.mainStream.removeAll();	
			for(int i=0;i<stringArray.length;i++){
				
				if(stringArray[i]!=null&&!stringArray[i].isEmpty()){					
					t = new Token();
					t.setTermText(stringArray[i]);
					super.mainStream.addToken(t);
				}
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		super.mainStream.reset();		
	}

	@Override
	public boolean increment() throws TokenizerException {		
		// TODO Auto-generated method stub		
		if(super.mainStream.hasNext()){
			super.mainStream.next();
			return true;
		}
		return false;
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		return super.mainStream;
	}

	private String[] filter(String tokenString) throws ParseException{			
		String[] stringArray = null;	
		stringArray = tokenString.split(" ");
		PorterAlgo pa = new PorterAlgo();
		int i;
		for (i=0;i<stringArray.length;i++)
		{
			if(stringArray[i].matches("^[A-Za-z]+$")){
				String s1 = pa.step1(stringArray[i]);
				String s2 = pa.step2(s1);
				String s3= pa.step3(s2);
				String s4= pa.step4(s3);
				String s5= pa.step5(s4);
				stringArray[i] = s5;
			}
		}
		return stringArray;	
	}

}
