/**
 * 
 */
package edu.buffalo.cse.irf14.analysis;

import java.util.Iterator;
import java.util.ArrayList;


/**
 * @author nikhillo
 * Class that represents a stream of Tokens. All {@link Analyzer} and
 * {@link TokenFilter} instances operate on this to implement their
 * behavior
 */
public class TokenStream implements Iterator<Token>{
	private ArrayList <Token> tokenList = null;
	private int index,secondaryIndex;
	public TokenStream(){
		tokenList = new ArrayList<Token>(); 		
		reset();		
	}
	/**
	 * Method that checks if there is any Token left in the stream
	 * with regards to the current pointer.
	 * DOES NOT ADVANCE THE POINTER
	 * @return true if at least one Token exists, false otherwise
	 */
	
	
	
	@Override
	public boolean hasNext() {
		// TODO YOU MUST IMPLEMENT THIS	
		Boolean result = false;	
		if(this.getListSize()-1> index && this.tokenList.get(index+1)!=null) {
			result = true;
		}
		return result;
	}
	

	/**
	 * Method to return the next Token in the stream. If a previous
	 * hasNext() call returned true, this method must return a non-null
	 * Token.
	 * If for any reason, it is called at the end of the stream, when all
	 * tokens have already been iterated, return null
	 */
	@Override
	public Token next() {
		// TODO YOU MUST IMPLEMENT THIS		
		Token t = null;
		secondaryIndex+=1;
		if(this.hasNext())
		 {		
			this.increamentIndex();
			t= this.tokenList.get(index);			
		 }
		return t;
	}
	
	
	private void increamentIndex(){
		if(this.getListSize()-1>=index+1){
			index +=1;		
		}
	}
	
	/**
	 * Method to remove the current Token from the stream.
	 * Note that "current" token refers to the Token just returned
	 * by the next method. 
	 * Must thus be NO-OP when at the beginning of the stream or at the end
	 */
	@Override
	public void remove() {
		// TODO YOU MUST IMPLEMENT THIS	
		if(index>-1){
			this.tokenList.set(index, null);
		}
	}
	
	/**
	 * Method to reset the stream to bring the iterator back to the beginning
	 * of the stream. Unless the stream has no tokens, hasNext() after calling
	 * reset() must always return true.
	 */
	public void reset() {
		//TODO : YOU MUST IMPLEMENT THIS
		index =-1;
		secondaryIndex = 0;
		
	}
	
	/**
	 * Method to append the given TokenStream to the end of the current stream
	 * The append must always occur at the end irrespective of where the iterator
	 * currently stands. After appending, the iterator position must be unchanged
	 * Of course this means if the iterator was at the end of the stream and a 
	 * new stream was appended, the iterator hasn't moved but that is no longer
	 * the end of the stream.
	 * @param stream : The stream to be appended
	 */
	public void append(TokenStream stream) {
		//TODO : YOU MUST IMPLEMENT THIS
		Token t = null;
		if(stream!=null){	
			stream.reset();	
			
			while(stream.hasNext()){	
				t = stream.next();				
				this.tokenList.add(t);												
			}
			
			
		}
	}

	public void addToken(Token token){
		
		if(token!=null){						
			this.tokenList.add(token);	
		
		}
	}
	
	public int getIndex(){
		return index;
	}
	
	public int getListSize(){
		return this.tokenList.size();
	}
	public void removeAll(){
		this.tokenList.clear();
		this.reset();
		
	}

	public Token getCurrent() {
		// TODO Auto-generated method stub
		Token t = null;
		if(index>-1&&this.getListSize()>=secondaryIndex){				
			t = this.tokenList.get(index);			
		}
		return t;
	}
}
