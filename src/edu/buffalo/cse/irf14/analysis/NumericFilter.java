package edu.buffalo.cse.irf14.analysis;



public class NumericFilter extends TokenFilter {
	
	

	public NumericFilter(TokenStream stream) {
		super(stream);
		// TODO Auto-generated constructor stub		
		super.mainStream.reset();
		
		String s = "";	
		String st = null;
		Token t = null;
		String[] stringArray = null;		
		int i = 0;


		try {
			while(increment()){			
			
				st = filter(super.mainStream.getCurrent().toString());
				if(st!=null)
					s=s+" "+st;
			}	


		} catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.mainStream.removeAll();
		stringArray = s.trim().split(" ");
		for(i=0;i<stringArray.length;i++){
			if(stringArray[i]!=null&&!stringArray[i].isEmpty()){
				t = new Token();
				t.setTermText(stringArray[i]);
				super.mainStream.addToken(t);
			}
		}
		super.mainStream.reset();		
	}

	@Override
	public boolean increment() throws TokenizerException {		
		// TODO Auto-generated method stub		
		if(super.mainStream.hasNext()){
			super.mainStream.next();
			return true;
		}
		return false;
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		return super.mainStream;
	}


	private String filter(String tokenString) throws TokenizerException{									
	
		if(tokenString.matches(".*\\d+.*")){
				if(tokenString.matches("\\d+\\,\\d+.*"))
				{
					return null;
				}
				else if(tokenString.matches("\\d+\\.*\\d+%"))
				{
					tokenString="%";
				}
				else if(tokenString.matches("\\d+/\\d+"))
				{
					tokenString="/";
				}
				else if(tokenString.matches("\\d+"))
				{
					if(tokenString.matches("\\d{8}")){
						//do nothing
					
				}else{
					return null;
				}
					
			}
		}		
		return tokenString;
		}
}
