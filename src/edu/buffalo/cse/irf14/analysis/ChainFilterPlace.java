package edu.buffalo.cse.irf14.analysis;

public class ChainFilterPlace implements Analyzer {
	private TokenStream mainStream = null;
	
	private static ChainFilterPlace singleton = new ChainFilterPlace( );
	private ChainFilterPlace(){
		
	}
	public static ChainFilterPlace getInstance() {
		//TODO: YOU NEED TO IMPLEMENT THIS METHOD
		return singleton;
	}
	
	public void filter(TokenStream stream) throws TokenizerException{
		TokenFilterFactory tokenFilterFactory = TokenFilterFactory.getInstance();
		mainStream = stream;
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.SPECIALCHARS, mainStream).getStream();		
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.ACCENT, mainStream).getStream();
		mainStream.reset();	
	}
	
	@Override
	public boolean increment() throws TokenizerException {
		if(mainStream.hasNext()){
			mainStream.next();
			return true;
		}
		return false;
		
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		return mainStream;
	}

}
