package edu.buffalo.cse.irf14.analysis;

import java.text.ParseException;

public class SpecialCharsFilter extends TokenFilter{

	public SpecialCharsFilter(TokenStream stream) {
		
		super(stream);
		// TODO Auto-generated constructor stub		
		super.mainStream.reset();
		
		String s = "";		
		Token t = null;
		String[] stringArray;
			try {
				while(increment()){			
					t = super.mainStream.getCurrent();
					s = s + " " +t.getTermText();
				}	
			} catch (TokenizerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			super.mainStream.removeAll();
			if(t!=null && s!=null){
				if(t.getTermText().matches(".*?\\p{Punct}.*") || s.contains("&") || s.matches(".*[><|\\\\$\\(\\)\"\\,\\[\\]].*"))
					try {					
						stringArray = this.filter(s.trim());		
						
						for(int i=0;i<stringArray.length;i++){
							if(stringArray[i]!=null&&!stringArray[i].isEmpty()){						
								t = new Token();
								t.setTermText(stringArray[i]);
								super.mainStream.addToken(t);
							}
						}
						
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else{
						stringArray = s.split(" ");
						
						for(int i=0;i<stringArray.length;i++){
							if(stringArray[i]!=null&&!stringArray[i].isEmpty()){
								
								t = new Token();
								t.setTermText(stringArray[i]);
								super.mainStream.addToken(t);
							}
					}
				}
			}	
		super.mainStream.reset();		
	}

	@Override
	public boolean increment() throws TokenizerException {		
		// TODO Auto-generated method stub		
		if(super.mainStream.hasNext()){
			super.mainStream.next();
			return true;
		}
		return false;
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		return super.mainStream;
	}


	private String[] filter(String tokenString) throws ParseException{			
	
		String[] stringArray = null;
		stringArray = tokenString.split(" ");
		int i;
		//Start
		for(i = 0 ; i < stringArray.length ; i++)
		{
		if(stringArray[i].contains("\""))
		{			
			stringArray[i] = stringArray[i].replaceAll("\"", "");	
		}
		
		if(stringArray[i].contains("$"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\$", "");
		}
		if(stringArray[i].contains("@"))
		{	if(stringArray[i].matches(".*[A-Za-z0-9]+@.*"))
			{
			stringArray[i] = stringArray[i].replaceAll("@", "");
			}
		}		
		if(stringArray[i].contains("<"))
		{
			stringArray[i] = stringArray[i].replaceAll("<", "");
		}
		if(stringArray[i].contains(">"))
		{
			stringArray[i] = stringArray[i].replaceAll(">", "");
		}
		if(stringArray[i].contains("^"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\^", "");
		}
		if(stringArray[i].contains("("))
		{
			stringArray[i] = stringArray[i].replaceAll("\\(", "");
		}
		if(stringArray[i].contains(")"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\)", "");
		}
		if(stringArray[i].contains("["))
		{
			stringArray[i] = stringArray[i].replaceAll("\\[", "");
		}
		if(stringArray[i].contains("]"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\]", "");
		}
		if(stringArray[i].contains("{"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\{", "");
		}
		if(stringArray[i].contains("}"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\}", "");
		}
		if(stringArray[i].contains("|"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\|", "");
		}
		if(stringArray[i].contains("\\"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\\\", "");
		}
		if(stringArray[i].contains("/"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\/", "");
		}
		if(stringArray[i].contains("_"))
		{	
			stringArray[i] = stringArray[i].replaceAll("_", "");
		}
		if(stringArray[i].contains("%"))
		{
			stringArray[i] = stringArray[i].replaceAll("%", "");
		}
		if(stringArray[i].contains("+"))
		{
			stringArray[i] = stringArray[i].replaceAll("\\+", "");
		}
		if(stringArray[i].contains("="))
		{
			
			stringArray[i] = stringArray[i].replaceAll("=", "");
			
		}
		if(stringArray[i].contains("#"))
		{
			stringArray[i] = stringArray[i].replaceAll("#", "");
		}
		if(stringArray[i].contains("*"))
		{
			
			stringArray[i] = stringArray[i].replaceAll("\\*", "");
			
		}
		if(stringArray[i].contains("~"))
		{
			stringArray[i] = stringArray[i].replaceAll("~", "");
		}
		
		if(stringArray[i].contains("&"))
		{
			stringArray[i] = stringArray[i].replaceAll("&", "");	
		}
		if(stringArray[i].contains("-"))
		{

			if(stringArray[i].matches(".*\\d.*")){
				//contains a digit retain full token
			}
			else{
				stringArray[i] = stringArray[i].replaceAll("-", "");	
			}
		}
		if(stringArray[i].contains(":"))
		{
			stringArray[i] = stringArray[i].replaceAll(":", "");	
		}
		if(stringArray[i].contains(";"))
		{
			stringArray[i] = stringArray[i].replaceAll(";", "");	
		}
		if(stringArray[i].contains(","))
		{
			stringArray[i] = stringArray[i].replaceAll("\\,", "");	
		}
		//End
	}
	
		return stringArray;
		}

}
