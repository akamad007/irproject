package edu.buffalo.cse.irf14.analysis;

public class ChainFilterContent  implements Analyzer {
	private TokenStream mainStream = null;
	
	private static ChainFilterContent singleton = new ChainFilterContent( );
	private ChainFilterContent(){
		
	}
	public static ChainFilterContent getInstance() {
		//TODO: YOU NEED TO IMPLEMENT THIS METHOD
		return singleton;
	}
	
	public void filter(TokenStream stream) throws TokenizerException{
		TokenFilterFactory tokenFilterFactory = TokenFilterFactory.getInstance();
		
		mainStream = stream;
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.STOPWORD, mainStream).getStream();		
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.STEMMER, mainStream).getStream();
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.SYMBOL, mainStream).getStream();
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.SPECIALCHARS, mainStream).getStream();				
		
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.ACCENT, mainStream).getStream();
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.DATE, mainStream).getStream();
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.NUMERIC, mainStream).getStream();
		mainStream.reset();
	}
	
	@Override
	public boolean increment() throws TokenizerException {
		if(mainStream.hasNext()){
			mainStream.next();
			return true;
		}
		return false;
		
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		return mainStream;
	}

}
