/**
 * 
 */
package edu.buffalo.cse.irf14.analysis;

import edu.buffalo.cse.irf14.document.FieldNames;

/**
 * @author nikhillo
 * This factory class is responsible for instantiating "chained" {@link Analyzer} instances
 */
public class AnalyzerFactory {
	/**
	 * Static method to return an instance of the factory class.
	 * Usually factory classes are defined as singletons, i.e. 
	 * only one instance of the class exists at any instance.
	 * This is usually achieved by defining a private static instance
	 * that is initialized by the "private" constructor.
	 * On the method being called, you return the static instance.
	 * This allows you to reuse expensive objects that you may create
	 * during instantiation
	 * @return An instance of the factory
	 */
	private static AnalyzerFactory singleton = new AnalyzerFactory( );
	private AnalyzerFactory(){
		
	}
	public static AnalyzerFactory getInstance() {
		//TODO: YOU NEED TO IMPLEMENT THIS METHOD
		return singleton;
	}
	
	/**
	 * Returns a fully constructed and chained {@link Analyzer} instance
	 * for a given {@link FieldNames} field
	 * Note again that the singleton factory instance allows you to reuse
	 * {@link TokenFilter} instances if need be
	 * @param name: The {@link FieldNames} for which the {@link Analyzer}
	 * is requested
	 * @param TokenStream : Stream for which the Analyzer is requested
	 * @return The built {@link Analyzer} instance for an indexable {@link FieldNames}
	 * null otherwise
	 */
	public Analyzer getAnalyzerForField(FieldNames name, TokenStream stream) {
		//TODO : YOU NEED TO IMPLEMENT THIS METHOD
		
		if(stream!=null){
		stream.reset();		
		
		switch(name){
		case FILEID:								
			return null;
		case TITLE:
			
			ChainFilterContent chnFltTitle=  ChainFilterContent.getInstance();	
			try {											
				 chnFltTitle.filter(stream);				
			} catch (TokenizerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return chnFltTitle;
		case CATEGORY:
			ChainFilterCategory chnFltCategory=  ChainFilterCategory.getInstance();	
			try {											
				 chnFltCategory.filter(stream);				
			} catch (TokenizerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return chnFltCategory;
		
		case AUTHORORG:
			ChainFilterContent chnFltAuthorOrg=  ChainFilterContent.getInstance();			
			try {											
				chnFltAuthorOrg.filter(stream);
				
				
			} catch (TokenizerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return chnFltAuthorOrg;
		case PLACE:
			ChainFilterPlace chnFltPlace=  ChainFilterPlace.getInstance();
			try {
				chnFltPlace.filter(stream);
			} catch (TokenizerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return chnFltPlace;
		case NEWSDATE:			
			ChainFilterContent chnFltNewsDate =  ChainFilterContent.getInstance();	
			try {
				chnFltNewsDate.filter(stream);
			} catch (TokenizerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return chnFltNewsDate;
		case CONTENT:
			ChainFilterContent chnFltContent=  ChainFilterContent.getInstance();
			try {
				chnFltContent.filter(stream);				
			} catch (TokenizerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			return chnFltContent;
		case AUTHOR:			
			ChainFilterAuthor chnFltAuthor=  ChainFilterAuthor.getInstance();		
			try {
				chnFltAuthor.filter(stream);
			} catch (TokenizerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return chnFltAuthor;
		default:
			return null;
		}
		
		
		}
		
		return null;
	}
}
