package edu.buffalo.cse.irf14.analysis;

public class ChainFilterCategory implements Analyzer {
	private TokenStream mainStream = null;
	
	private static ChainFilterCategory singleton = new ChainFilterCategory( );
	private ChainFilterCategory(){
		
	}
	public static ChainFilterCategory getInstance() {
		//TODO: YOU NEED TO IMPLEMENT THIS METHOD
		return singleton;
	}
	
	public void  filter(TokenStream stream) throws TokenizerException{
		TokenFilterFactory tokenFilterFactory = TokenFilterFactory.getInstance();
		mainStream = stream;				
		mainStream = tokenFilterFactory.getFilterByType(TokenFilterType.SYMBOL, mainStream).getStream();				
		mainStream.reset();
	}
	
	@Override
	public boolean increment() throws TokenizerException {
		if(mainStream.hasNext()){
			mainStream.next();
			return true;
		}
		return false;
		
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		return mainStream;
	}

}


