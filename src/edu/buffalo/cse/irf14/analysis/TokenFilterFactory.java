/**
 * 
 */
package edu.buffalo.cse.irf14.analysis;




/**
 * Factory class for instantiating a given TokenFilter
 * @author nikhillo
 *
 */
public class TokenFilterFactory {
	/**
	 * Static method to return an instance of the factory class.
	 * Usually factory classes are defined as singletons, i.e. 
	 * only one instance of the class exists at any instance.
	 * This is usually achieved by defining a private static instance
	 * that is initialized by the "private" constructor.
	 * On the method being called, you return the static instance.
	 * This allows you to reuse expensive objects that you may create
	 * during instantiation
	 * @return An instance of the factory
	 */
	private static TokenFilterFactory singleton = new TokenFilterFactory( );
	private TokenFilterFactory(){
		
	}
	public static TokenFilterFactory getInstance() {
		//TODO: YOU NEED TO IMPLEMENT THIS METHOD
		return singleton;
	}
	
	
	/**
	 * Returns a fully constructed {@link TokenFilter} instance
	 * for a given {@link TokenFilterType} type
	 * @param type: The {@link TokenFilterType} for which the {@link TokenFilter}
	 * is requested
	 * @param stream: The TokenStream instance to be wrapped
	 * @return The built {@link TokenFilter} instance
	 * @throws TokenizerException 
	 */
	public TokenFilter getFilterByType(TokenFilterType type, TokenStream stream) throws TokenizerException {
		//TODO : YOU MUST IMPLEMENT THIS METHOD
		switch(type){		
			case SYMBOL: 
				SymbolFilter sf = new SymbolFilter(stream);
				return sf;
			case DATE:
				DateFilter date = new DateFilter(stream);
				date.increment();
				return date;
			case NUMERIC:
				NumericFilter num = new NumericFilter(stream);
				return num;
			case CAPITALIZATION:
				CapitalizationFilter capitalization = new CapitalizationFilter(stream);
				return capitalization;
			case STOPWORD:				
				StopwordFilter sw = new StopwordFilter(stream);				
				return sw;
			case STEMMER:
				StemmerFilter stem = new StemmerFilter(stream);
				return stem;
			case ACCENT:
				AccentFilter accent = new AccentFilter(stream);
				return accent;
			case SPECIALCHARS:
				SpecialCharsFilter sc = new SpecialCharsFilter(stream);
				return sc;
			default:
				return null;
		}
		
	}
}

















