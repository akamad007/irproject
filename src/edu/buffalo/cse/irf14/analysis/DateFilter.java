package edu.buffalo.cse.irf14.analysis;

public class DateFilter extends TokenFilter{


	public DateFilter(TokenStream stream){
		super(stream);
		// TODO Auto-generated constructor stub		
		super.mainStream.reset();
		String s = "";		
		Token t = null;
		String[] stringArray = null;		
		int i = 0;

		
		try {
			while(increment()){			
				t = super.mainStream.getCurrent();
				s = s + " " +t.getTermText();
			}	


		} catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			stringArray = filter(s.trim());
		} catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(Exception e){
			stringArray = null;
		}
		super.mainStream.removeAll();
		
		if(stringArray!=null){
			for(i=0;i<stringArray.length;i++){
				if(stringArray[i]!=null&&!stringArray[i].isEmpty()){
					t = new Token();
					t.setTermText(stringArray[i]);
					super.mainStream.addToken(t);
				}
			}
		}
		super.mainStream.reset();		
	}

	@Override
	public boolean increment() throws TokenizerException {		
		// TODO Auto-generated method stub		
		if(super.mainStream.hasNext()){
			super.mainStream.next();
			return true;
		}
		return false;
	}

	@Override
	public TokenStream getStream() {
		// TODO Auto-generated method stub
		return super.mainStream;
	}


	private String[] filter(String tokenString) throws TokenizerException{									
		
		String newTemp="";
		String[] temp = null;
		String month ="01";
		String day = "01";
		String[] months={
				"January","February","March","April","May","June","July",
				"August","September","October","November","December"
		};
		String year ="0000";
		int i;
		String line = tokenString;
		//----------Start----------------
		
		if(line.matches(".*\\d.*")){
			temp = line.split(" ");
			int j = 0;
			for(i=0;i<temp.length;i++){
				day = "01";
				month = "01";
				year="1900";


				if(temp[i].matches(".*\\d+.*")){
					String temp1 = "";
					
					//AD
					if(temp[i].matches(".*\\d+AD.*")){
						//there is a dot after AD so we have to deal with that as well
						temp1 = temp[i].replaceAll("\\D", "");
						year = String.format("%04d", Integer.parseInt(temp1));
						if(temp[i].matches(".*\\.$"))
						{
							day = day + ".";
						}
						temp[i]=year + month + day;
					}//AD ends
					
					// Ad without dot
					if(i<temp.length-1)
						if(temp[i+1].matches("AD.*")){
							year = String.format("%04d", Integer.parseInt(temp[i]));
							temp[i+1]="";
							if(temp[i+1].matches(".*\\.$"))
							{
								day = day + ".";
							}
							temp[i]=year + month + day;
						}// Ad without dot ends
					
					//BC
					if(temp[i].matches(".*\\d+BC.*")){
						temp1 = temp[i].replaceAll("\\D", "");
						
						year = String.format("%04d", Integer.parseInt(temp1));
						if(temp[i].matches(".*\\.$"))
						{
							day = day + ".";
						}
						temp[i]="-" + year + month + day;
					}//BC ends
					
					
					//BC with a dot
					if(i<temp.length-1){
						if(temp[i+1].matches("BC.*")){
							year = temp[i].replaceAll("\\D", "");
							year = String.format("%04d", Integer.parseInt(year));
							
							temp[i+1]="";
							if(temp[i+1].matches(".*\\.$"))
							{
								day = day + ".";
							}
							temp[i]="-" + year + month + day;
						}
						}//BC dot ends					
					

					if(i<=temp.length-2)
					{
						
						if((i<temp.length-1 && temp[i+1].matches("\\w+.*")) || temp[i-1].matches("\\w+.*"))
						{  
							//test if no. days is less than 31
							for(j=0;j<12;j++){
								
								if((i+1<temp.length)&&temp[i+1].contains(months[j])){
									month = String.format("%02d", j+1);
									
									
									if((i+2<temp.length)&&temp[i+2].matches("\\d+.*")){//this is for year
										year = temp[i+2].replaceAll("\\D", "");
										year = String.format("%04d", Integer.parseInt(year));

									}
									else{
										year="1900";
									}
									
									
									day = temp[i].replaceAll("\\D", "");
									day = String.format("%02d", Integer.parseInt(day));
									newTemp = year + month + day;  
								
									if((i+2<temp.length)&&temp[i+2].matches(".*\\,$") || temp[i+1].matches(".*\\,$"))
									{

										newTemp = newTemp + ",";
									}
									
									else if((i+2<temp.length)&&temp[i+2].matches(".*\\.$"))
									{

										newTemp = newTemp + ".";
									}
									
									else if((i+2<temp.length)&&temp[i+2].matches(".*\\?$"))
									{

										newTemp = newTemp + "?";
									}
									
									else if((i+2<temp.length)&&temp[i+2].matches(".*\\!$"))
									{

										newTemp = newTemp + "!";
									}


									temp[i] = newTemp;

									temp[i] = newTemp;
									temp[i+1] = "";
									if((i+2<temp.length)&&temp[i+2].matches("\\d+.*"))
										temp[i+2] = "";	

									break;
								}//end of after day
								
								
								
								if(i>0)//month is before day
									if(temp[i-1].contains(months[j]))
									{	
										//test if no. days is less than 31
										month = String.format("%02d", j+1);
										
										
										if(temp.length-1 > i+1 && temp[i+1].matches("\\d+.*")){//this is for year
											year = temp[i+1].replaceAll("\\D", "");
											if(year.length()<5)
											year = String.format("%04d", Integer.parseInt(year));
											else{
												year="1900";
											}
										}
										else
										{
											year="1900";
										}
										
										day = temp[i].replaceAll("\\D", "");

										day = String.format("%02d", Integer.parseInt(day));
										
										newTemp = year + month + day;
										
										if(temp.length-1 > i+1 && temp[i+1].matches(".*\\,$"))
										{

											newTemp = newTemp + ",";
										}
										else if(temp.length-1 > i+1 && temp[i+1].matches(".*\\.$"))
										{

											newTemp = newTemp + ".";
										}
										else if(temp.length-1 > i+1 && temp[i+1].matches(".*\\?$"))
										{

											newTemp = newTemp + "?";
										}
										else if(temp.length-1 > i+1 && temp[i+1].matches(".*\\!$"))
										{

											newTemp = newTemp + "!";
										}

										temp[i] = newTemp;
										temp[i-1] = "";
										if(temp.length-1 > i+1 && temp[i+1].matches("\\d+.*"))
											temp[i+1] = "";

										break;
									}
							}


						}
				}
					if(temp[i].matches("\\d{4}"))
					{
						temp[i] = temp[i].replaceAll("\\D", "");
						temp[i] = temp[i] + "01" + "01";

					}
					//matches 2012-14 type
					if(temp[i].matches(".*\\d+-\\d+.*")){
						String[] arrString = null;
						arrString = temp[i].split("-");
						String year1="" , year2="";
						String date1="" , date2="";
						temp1 = "";
						if(arrString[0].matches("\\d{4}"))
						{
							year1 = arrString[0]; 
						}
						temp1 = arrString[1].replaceAll("\\D", "");
						if(temp1.matches("\\d{2}"))
						{
							
							if(arrString[0].length()>2)
							year2 = arrString[0].substring(0, 2) + temp1;
						}

						date1 = year1 + "01" + "01";
						date2 = year2 + "01" + "01";
						if(arrString[1].matches(".*\\.$"))
						{
							date2 = date2 + ".";
						}

						temp[i] = date1 + "-" + date2;
					}//matches 2012-14 type ends--------

					//for time

					if(temp[i].contains(":"))
					{

						String[] arrString;
						arrString = temp[i].split(":");
						String hour="00";
						String min="00";
						String sec="00";
						temp1 ="";
						String time ="";
						int tempHour;
						hour = arrString[0];
						if(arrString[1].matches("\\d+\\w+.*"))
						{
							if(arrString[1].matches(".*pm.*") || arrString[1].matches(".*PM.*"))
							{
								temp1 = arrString[1].replaceAll("\\D", "");
								tempHour = Integer.parseInt(hour) + 12;
								hour = "" + tempHour;
							}
							min = temp1;
							sec = "00";
							time = hour + ":" + min + ":" + sec;
							if(temp[i].matches(".*\\.$"))
								time = time + ".";
						}

						if(arrString[1].matches("\\d+"))
						{

							if(temp[i+1].matches(".*pm.*") || temp[i+1].matches(".*PM.*"))
							{
								tempHour = Integer.parseInt(hour) + 12;
								hour = "" + tempHour;
							}
							min = arrString[1];
							sec = "00";
							time = hour + ":" + min + ":" + sec;
							if(temp[i+1].matches(".*\\.$"))
							{
								time = time + ".";
							}
							temp[i+1]="";
						}
						temp[i]=time;

					}//end time

					

				}
			}
		}
		else{
			temp = line.split(" ");
			
		}
		
		//----------End------------------
		
		String finalString ="";
		for(int k=0;k<temp.length;k++)
		{   

			if(temp[k]!="")
				finalString = finalString + " " + temp[k];
		}
		finalString = finalString.trim();

		return temp;
	}
} 




