package edu.buffalo.cse.irf14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

import edu.buffalo.cse.irf14.analysis.Analyzer;
import edu.buffalo.cse.irf14.analysis.AnalyzerFactory;
import edu.buffalo.cse.irf14.analysis.TokenStream;
import edu.buffalo.cse.irf14.analysis.Tokenizer;
import edu.buffalo.cse.irf14.analysis.TokenizerException;
import edu.buffalo.cse.irf14.document.FieldNames;
import edu.buffalo.cse.irf14.index.IndexReader;
import edu.buffalo.cse.irf14.index.IndexType;
import edu.buffalo.cse.irf14.query.Query;
import edu.buffalo.cse.irf14.query.QueryParser;
import edu.buffalo.cse.irf14.query.QueryType;
import edu.buffalo.cse.irf14.relevance.OkapiModel;
import edu.buffalo.cse.irf14.relevance.TfIdfModel;

/**
 * Main class to run the searcher.
 * As before implement all TODO methods unless marked for bonus
 * @author nikhillo
 *
 */
public class SearchRunner {
	public enum ScoringModel {TFIDF, OKAPI};
	
	/**
	 * Default (and only public) constuctor
	 * @param indexDir : The directory where the index resides
	 * @param corpusDir : Directory where the (flattened) corpus resides
	 * @param mode : Mode, one of Q or E
	 * @param stream: Stream to write output to
	 */
	String indexDir;
	String corpusDir;
	IndexReader termIndexReader,categoryIndexReader, authorIndexReader,placeIndexReader;
	char mode;
	PrintStream stream;
	String defaultOperator;
	private Integer avgDocLength; 
	private List <String> searchTermsQuery = new ArrayList<String>();
	private Integer totalDocuments;
	
	public SearchRunner(String indexDir, String corpusDir, 
			char mode, PrintStream stream) {
		this.indexDir = indexDir;
		this.corpusDir = corpusDir;
		this.mode = mode;
		this.stream = stream;
		termIndexReader =  new IndexReader(this.indexDir,IndexType.TERM);
		categoryIndexReader =  new IndexReader(this.indexDir,IndexType.CATEGORY);
		authorIndexReader=  new IndexReader(this.indexDir,IndexType.AUTHOR);
		placeIndexReader=  new IndexReader(this.indexDir,IndexType.PLACE);
		avgDocLength = IndexReader.getAverageDocumentLength();
		totalDocuments = IndexReader.getTotalDocuments();
		defaultOperator = "OR";					
	}
	
	/**
	 * Method to execute given query in the Q mode
	 * @param userQuery : Query to be parsed and executed
	 * @param model : Scoring Model to use for ranking results
	 */
	public void query(String userQuery, ScoringModel model) {
		//TODO: IMPLEMENT THIS METHOD			
		long startTime = System.currentTimeMillis();			 
		Query queryObject = QueryParser.parse(userQuery, defaultOperator);					
		List<Integer> result = processQuery(queryObject);
		
		applyRelevanceModel(result,model,null,queryObject.rawTerms);
		long endTime = System.currentTimeMillis();

		long duration = (endTime - startTime);
		stream.println("Total time for query:"+duration+" milli seconds");
	}
	
	private void applyRelevanceModel(List<Integer> result, ScoringModel model,String queryID,List<String> rawTerms){
		
		if(result!=null){
		IndexReader reader = null;
		HashMap<Integer,Double> docRlevance = new HashMap<Integer,Double>();
		HashMap<String,List<Integer>> tf = new HashMap<String,List<Integer>>(); 
		
		List<Integer> df = new ArrayList<Integer>();
		List<String> terms = new ArrayList<String>();
		List<Integer> lD = new ArrayList<Integer>();
		List<HashMap<Integer,Integer>> inversDocTerm = new ArrayList<HashMap<Integer,Integer>> ();
		for(Integer docId:result){		
			lD.add(termIndexReader.getDocumentLength(docId));
		}
		
			for(String termAndIndex:searchTermsQuery)
			{
				String term = termAndIndex.split("!")[0];
				String index = termAndIndex.split("!")[1];
				List<Integer> tfList = new ArrayList<Integer>();
				terms.add(term);
				
				if(index.equals("TERM"))
				{
					reader = termIndexReader;
				} 
				else{
						if(index.equals("PLACE")){
							reader = placeIndexReader;							
						}
						else{
							if(index.equals("CATEGORY")){
								reader = categoryIndexReader;
							}
							else{
								if(index.equals("AUTHOR")){
									reader = authorIndexReader;
								}
							}
						}
					} 
				
				for(Integer docId:result){							
						tfList.add(reader.getTermFrequency(term, docId));												
					}
				
				tf.put(term,tfList);
				try{
					df.add(reader.getPostings(term).size());	
				}catch(NullPointerException e){
					df.add(0);
				}
			}
			if(model == ScoringModel.OKAPI)
			{		
				OkapiModel okapi = new OkapiModel();
				docRlevance = okapi.getRelevanceScore(terms, result, tf, df, totalDocuments, lD, avgDocLength);				
			}
		else{
				if(model == ScoringModel.TFIDF){
					TfIdfModel tfidf = new TfIdfModel();									
					docRlevance = tfidf.getCosineScore(terms, result, tf, df, totalDocuments);						
				}
			}				
							 				 
			 Map<String,Double> finalResult = new  HashMap<String,Double>();
			
			 for(Entry<Integer,Double> h:docRlevance.entrySet()){
				
				
					 finalResult.put(IndexReader.getDocumentName(h.getKey()), h.getValue());
				 
				 
			 }
			 finalResult = sortByComparator(finalResult,false);
			
			 
			 if(result !=null)
				{
				 	
					if(queryID!=null)
					{			
						int i=0;
						
						String finalString = "";
						String [] temp ;
						Double max = 0.0;
						for(Entry<String, Double> h:finalResult.entrySet()){
							if(i==0){
								max = h.getValue();
							}
							temp = IndexReader.getDocumentTitleContent(rawTerms,h.getKey(),corpusDir);
							if(temp[1]!=""){
								
								finalString = finalString+h.getKey()+"="+ ((double)Math.round((h.getValue()/(max)) * 10) / 10)+",";
										if(i>=9){
											break;
										}
								i++;
							}
						}
						
						
						finalString="{"+finalString+"}";
						finalString = finalString.replace(",}", "}");
						stream.println(queryID+""+finalString.replaceAll("=", "#"));
					}
					else{
						String [] temp ;
						int i =0;
						Double max = 0.0;
						for(Entry<String, Double> h:finalResult.entrySet()){
							temp = IndexReader.getDocumentTitleContent(rawTerms,h.getKey(),corpusDir);
							if(temp[1]!=""){
								if(i==0){
									max = h.getValue();
								}
								stream.println("Document Name:"+h.getKey()+" Document Weight:"+ ((double)Math.round((h.getValue()/(max)) * 10) / 10));
								stream.println("Title :"+temp[0]);
								stream.println("Snippet :"+temp[1]);
								stream.println("\n");
								i++;if(i>9){break;}
							}
						}
					}
				}
			
		}
		else{
			if(queryID!=null)
				stream.println(queryID+":"+"null");
			else
				stream.println("null");
			}
	}
	
    private static Map<String, Double> sortByComparator(Map<String, Double> unsortMap, final boolean order)
    {

        List<Entry<String, Double>> list = new LinkedList<Entry<String, Double>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Entry<String, Double>>()
        {
            public int compare(Entry<String, Double> o1,
                    Entry<String, Double> o2)
            {
                if (order)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        for (Entry<String, Double> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
	
	
	/**
	 * Method to execute queries in E mode
	 * @param queryFile : The file from which queries are to be read and executed
	 */
	public void query(File queryFile) {
		//TODO: IMPLEMENT THIS METHOD
		
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(queryFile);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String encoding =  "UTF-8";
		BufferedReader br = null;
		try {
			br = new BufferedReader( new InputStreamReader(fis, encoding ));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			while(( line = br.readLine()) != null ) {
				sb.append( line );
				sb.append( '\n' );
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String queryDoc = sb.toString();
		
		int i=0;
		while(queryDoc.charAt(i)!='=')
		{
			i++;
		}
		//int numberOfQuery = Character.getNumericValue(queryDoc.charAt(i+1));
		String nOfQuery = "";
		i++;
		while(queryDoc.charAt(i)!='\n'){
			 nOfQuery=nOfQuery + queryDoc.charAt(i);
			 i++;
		}
		int numberOfQuery = Integer.parseInt(nOfQuery);
			int srNo=0;
			String queries= "";
			int queryNo=0;
			String temp="";
			String queryID="";
			
			for(i=i+2;i<queryDoc.length();i++)
			{
				if(queryDoc.charAt(i)=='{')
				{
					i++;
					while(queryDoc.charAt(i)!='}')
					{
						temp = temp + queryDoc.charAt(i);
						i++;
					}
					if(!queries.equals(""))
						queries = queries + "<___>" + temp.trim();
					else
						queries = temp.trim();	

					queryNo++;
					temp="";
					if(queryNo>=numberOfQuery)
						break;
				}
				else{
					
					queryID = queryID + queryDoc.charAt(i);
				}
				
			}		
			queryID = queryID.trim(); 
			String[] queryIDArray = queryID.split("\n");
			ArrayList<Integer> matchedDocs = processEQueries(queries,queryIDArray);//docs matched for given terms		
	}
	
	/**
	 * General cleanup method
	 */
	public void close() {
		//TODO : IMPLEMENT THIS METHOD
	}
	
	/**
	 * Method to indicate if wildcard queries are supported
	 * @return true if supported, false otherwise
	 */
	public static boolean wildcardSupported() {
		//TODO: CHANGE THIS TO TRUE ONLY IF WILDCARD BONUS ATTEMPTED
		return false;
	}
	
	/**
	 * Method to get substituted query terms for a given term with wildcards
	 * @return A Map containing the original query term as key and list of
	 * possible expansions as values if exist, null otherwise
	 */
	public Map<String, List<String>> getQueryTerms() {
		//TODO:IMPLEMENT THIS METHOD IFF WILDCARD BONUS ATTEMPTED
		return null;
		
	}
	
	/**
	 * Method to indicate if speel correct queries are supported
	 * @return true if supported, false otherwise
	 */
	public static boolean spellCorrectSupported() {
		//TODO: CHANGE THIS TO TRUE ONLY IF SPELLCHECK BONUS ATTEMPTED
		return false;
	}
	
	/**
	 * Method to get ordered "full query" substitutions for a given misspelt query
	 * @return : Ordered list of full corrections (null if none present) for the given query
	 */
	public List<String> getCorrections() {
		//TODO: IMPLEMENT THIS METHOD IFF SPELLCHECK EXECUTED
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private ArrayList<Integer> processQuery(Query queryObject)
	{
		String postFix = queryObject.toPostFix();
		
		String[] postFixArray = postFix.split(" ");
		IndexReader iReader = termIndexReader;
		ArrayList<Integer> docList;
		searchTermsQuery.clear();
		Stack<ArrayList> docStack = new Stack<ArrayList>();
		int i = 0;		
		
		for(i=0;i<postFixArray.length;i++)
		{
			if(postFixArray[i].equals("AND") || postFixArray[i].equals("OR") || postFixArray[i].equals("NOT"))
			{
				ArrayList<Integer> list2 = docStack.pop();
				ArrayList<Integer> list1 = docStack.pop();
				ArrayList<Integer> result =  new ArrayList<Integer>();
				if(postFixArray[i].equals("AND")){
					result = iReader.intersection(list1, list2);
				}
				if(postFixArray[i].equals("OR")){
					result = iReader.union(list1, list2);
				}
				if(postFixArray[i].equals("NOT")){
					result = iReader.not(list1, list2);
				}
				docStack.push(result);
			}
			else if(postFixArray[i].contains(":"))
			{
				
				String[] h = postFixArray[i].split(":");
				
				String index = h[0];
				String termToSearch = h[1];
				iReader = getReader(index);
				termToSearch = getTermToSearch(index,termToSearch);												
				docList = iReader.getDocIdList(termToSearch);	
				if(docList==null){
					termToSearch = getTermToSearch(index,capitalizeString(h[1]));
					docList = iReader.getDocIdList(termToSearch);
				}
				if(docList==null){
					termToSearch = getTermToSearch(index,h[1].toUpperCase());
					docList = iReader.getDocIdList(termToSearch);
				}
				searchTermsQuery.add(termToSearch+"!"+index.toUpperCase());	
				docStack.push(docList);
				
			}
		}		
		
		return docStack.pop();
	}
	public static String capitalizeString(String string) {
		  char[] chars = string.toLowerCase().toCharArray();
		  boolean found = false;
		  for (int i = 0; i < chars.length; i++) {
		    if (!found && Character.isLetter(chars[i])) {
		      chars[i] = Character.toUpperCase(chars[i]);
		      found = true;
		    } else if (Character.isWhitespace(chars[i]) || chars[i]=='.' || chars[i]=='\'') { // You can add other chars here
		      found = false;
		    }
		  }
		  return String.valueOf(chars);
		}
	private IndexReader getReader(String index){
		IndexReader iReader = termIndexReader;
		
		if(index.toUpperCase().equals("TERM")){
			iReader = termIndexReader;
		
		}
		if(index.toUpperCase().equals("CATEGORY")){
			iReader = categoryIndexReader;
			
		}
		if(index.toUpperCase().equals("AUTHOR")){
			iReader = authorIndexReader;
			
		}
		if(index.toUpperCase().equals("PLACE")){
			iReader = placeIndexReader;
			
		}	
		return iReader;
	}
	private String getTermToSearch(String index,String termToSearch){
		if(index.toUpperCase().equals("TERM")){
			
			termToSearch = getAnalyzedTerm(termToSearch,FieldNames.CONTENT);
		}
		if(index.toUpperCase().equals("CATEGORY")){
			
			termToSearch = getAnalyzedTerm(termToSearch,FieldNames.CATEGORY);
		}
		if(index.toUpperCase().equals("AUTHOR")){
			
			termToSearch = getAnalyzedTerm(termToSearch,FieldNames.AUTHOR);
		}
		if(index.toUpperCase().equals("PLACE")){
		
			termToSearch = getAnalyzedTerm(termToSearch,FieldNames.PLACE);
		}	
		
		return termToSearch;
	}
	
	private static String getAnalyzedTerm(String string, FieldNames type) {
		Tokenizer tknizer = new Tokenizer();
		AnalyzerFactory fact = AnalyzerFactory.getInstance();
		try {
			TokenStream stream = tknizer.consume(string);
			Analyzer analyzer = fact.getAnalyzerForField(type, stream);
			
			while (analyzer.increment()) {
				
			}
			stream = analyzer.getStream();
			if(stream!=null)
			{stream.reset();
			return stream.next().toString();}
			
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private ArrayList<Integer> processEQueries(String queries,String[] queryIDArray){

		String[] queryArray = queries.split("<___>");
		int i =0;
		
		ArrayList<Integer> result = null;
		stream.println(" ");
		stream.println("numResults="+queryIDArray.length);
		
		for(i=0;i<queryArray.length;i++) //process all queries
		{
			long startTime = System.currentTimeMillis();				
			Query queryObject = QueryParser.parse(queryArray[i], defaultOperator);
			result = processQuery(queryObject);
			
			applyRelevanceModel(result,ScoringModel.OKAPI,queryIDArray[i],queryObject.rawTerms);
			long endTime = System.currentTimeMillis();
			long duration = (endTime - startTime);
			stream.println("Total time for query:"+duration+" milli seconds");
			stream.println(" ");
			
		}
		return result;
	}
}



