package edu.buffalo.cse.irf14.query;


class Node  
{  
	public String data;  
	public Node leftChild;  
	public Node rightChild;  
	public Node(String x)  
	{  
		data=x;  
	}  
	public void displayNode()  
	{  
		System.out.print(data + " ");  
	}  
}  


class Stack1  
{  
	private Node[] a;  
	private int top,m;  
	public Stack1(int max)  
	{  
		m=max;  
		a=new Node[m];  
		top=-1;  
	}  
	public void push(Node key)  
	{  
		a[++top]=key;  
	}  
	public Node pop()  
	{  
		return(a[top--]);  
	}  
	public boolean isEmpty()  
	{  
		return (top==-1);  
	}  
}  

class Stack2  
{  
	private String[] a;  
	private int top,m;  
	public Stack2(int max)  
	{  
		m=max;  
		a=new String[m];  
		top=-1;  
	}  
	public void push(String ch)  
	{  
		a[++top]=ch;  
	}  
	public String pop()  
	{  
		return(a[top--]);  
	}  
	public boolean isEmpty()  
	{  
		return (top==-1);  
	}  
}  

class Conversion  
{  
	private Stack2 s;  
	private String[] input;  
	private String output="";  

	public Conversion(String str)  
	{  
		input=str.split(" ");  
		s=new Stack2(input.length);  
	}  

	public String inToPost()  
	{  
		for(int i=0;i<input.length;i++)  
		{  
			String ch=input[i];
			if(ch.equals("AND") || ch.equals("OR") || ch.equals("NOT"))  
			{
				gotOperator(ch);

			}
			else if(ch.equals("(")){
				s.push(ch); 

			}
			else if(ch.equals(")")){
				gotParenthesis();  

			}
			else{
				output=output + " " + ch;
			}  
			output = output.trim();
		}  

		while(!s.isEmpty())  
			output=output+ " " + s.pop();  
		return output;  
	}  


	private void gotOperator(String opThis)  
	{  
		String ch;
		while(!s.isEmpty()){
			ch = s.pop();
			if(ch.equals("AND") || ch.equalsIgnoreCase("OR") || ch.equalsIgnoreCase("NOT"))
			{
				output = output + " " + ch;
			}
			else if(ch.equals("("))
			{
				s.push(ch);
				break;
			}
		}
		s.push(opThis);
	}  


	private void gotParenthesis()  
	{  

		while(!s.isEmpty())  
		{  

			String ch=s.pop();  
			if(ch.equals("("))
			{

				break;  
			}
			else  
				output=output + " " +  ch;  
		}  
	}  
}//end   

class Tree  
{  
	private Node root;  
	public Tree()  
	{  
		root=null;  
	}  
	public void insert(String s)  
	{  
		Conversion c=new Conversion(s);  
		s=c.inToPost();  
		String[] arrString = s.split(" ");
		Stack1 stk=new Stack1(arrString.length);  

		int i=0;  
		Node newNode;  
		for(int j=0;j<arrString.length;j++)  
		{  
			if(arrString[j].equals("AND") ||arrString[j].equals("OR") ||arrString[j].equals("NOT"))  
			{  
				Node ptr1=stk.pop();  
				Node ptr2=stk.pop();  
				newNode=new Node(arrString[j]);  
				newNode.leftChild=ptr2;  
				newNode.rightChild=ptr1;  
				stk.push(newNode);

			}  
			else  
			{  

				newNode=new Node(arrString[j]);  
				stk.push(newNode);

			}  
		}  
		root=stk.pop();  
	}  
	public void traverse(int type)  
	{  
		switch(type)  
		{  
		case 1: System.out.print("Preorder Traversal:-    ");  
		preOrder(root);  
		break;  
		case 2: System.out.print("Inorder Traversal:-     ");  
		inOrder(root);  
		break;  
		case 3: System.out.print("Postorder Traversal:-   ");  
		postOrder(root);  
		break;  
		default:System.out.println("Invalid Choice");  
		}  
	}  
	private void preOrder(Node localRoot)  
	{  
		if(localRoot!=null)  
		{  
			localRoot.displayNode();  
			preOrder(localRoot.leftChild);  
			preOrder(localRoot.rightChild);  
		}  
	}  
	private void inOrder(Node localRoot)  
	{  
		if(localRoot!=null)  
		{  
			inOrder(localRoot.leftChild);  
			localRoot.displayNode();  
			inOrder(localRoot.rightChild);  
		}  
	}  
	private void postOrder(Node localRoot)  
	{  
		if(localRoot!=null)  
		{  
			postOrder(localRoot.leftChild);  
			postOrder(localRoot.rightChild);  
			localRoot.displayNode();  
		}  
	}  
}//end tree class 
