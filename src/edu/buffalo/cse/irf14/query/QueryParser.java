/**
 * 
 */
package edu.buffalo.cse.irf14.query;


/**
 * @author nikhillo
 * Static parser that converts raw text to Query objects
 */
public class QueryParser {
	/**
	 * MEthod to parse the given user query into a Query object
	 * @param userQuery : The query to parse
	 * @param defaultOperator : The default operator to use, one amongst (AND|OR)
	 * @return Query object if successfully parsed, null otherwise
	 */	
	public static Query parse(String userQuery, String defaultOperator) {
		//TODO: YOU MUST IMPLEMENT THIS METHOD
		Tree t1=new Tree();  
		String a= userQuery;
		a= a.replaceAll("\\(", "\\( ");
		a= a.replaceAll("\\)", " \\)");
		a=a.replaceAll("  ", " ");

		if(a.contains("\""))
			a = checkQuotes(a);

		//category:(a or b or c) type queries
		if(a.contains(":("))
			a = checkIndex(a); 

		String  complete = processBrackets(a,defaultOperator);
		String postFix = complete; 
		postFix = postFix.replaceAll("_", " ");
		complete  = handleNot(complete);

		String output1 = complete.replace("( ", "[");
		output1 = output1.replace(" )", "]");
		output1 = "{ " + output1 + " }";
		output1 = output1.replaceAll("_", " ");

		
		Conversion c = new Conversion(postFix);
		
		postFix = c.inToPost();
		t1.insert(complete); //insert into tree  
		Query q = new Query(output1,postFix);
/*
		t1.traverse(1);  

		t1.traverse(2);  

		t1.traverse(3);  
*/


		return q;
	}

	private static String checkQuotes(String a)
	{
		int i = 0;

		char[] b = a.toCharArray();

		for(i=0;i<a.length();i++)
		{
			if(b[i] == '"')
			{
				//	b[i]='\0';
				i++;
				while(i<a.length())
				{
					if(b[i]=='"')
					{
						break;
					}
					if(b[i]==' ')
					{
						b[i]='_';	
					}
					i++;
				}
			}
			a = new String(b);
			//	a = a.replaceAll("\"", "");
			a = a.trim();
		}//------------------------------------------------------


		return a;
	}

	private static String checkIndex(String a)
	{

		String bracketTerm="";
		String[] stringArray = a.split(" ");
		a="";
		int flag = 0;
		int i = 0;

		for(i=0;i<stringArray.length;i++)
		{
			if(stringArray[i].contains(":("))
			{
				bracketTerm = stringArray[i].replace("(", "");
				stringArray[i]="(";
				flag = 1; // category:( found
				continue;
			}
			if(flag==1)
			{
				flag=0;
				while(!stringArray[i].equals(")"))
				{
					if(!stringArray[i].equals("AND")&&!stringArray[i].equals("OR")&&!stringArray[i].equals("NOT"))
					{
						if(stringArray[i].matches("[a-zA-Z]*"))
						{
							stringArray[i] = bracketTerm+stringArray[i];
						}
					}
					i++;
					if(i>=stringArray.length)
						break;
				}
			}
			if(i>=stringArray.length)
				break;

		}
		for(i=0;i<stringArray.length;i++)
		{
			a = a + " " + stringArray[i];
		}
		a=a.trim();
		return a;
	}

	private static String processBrackets(String a, String defaultOperator)
	{

		int i = 0;
		String[] stringArray = a.split(" ");
		int stringLength = stringArray.length;
		//1. Puts Term: index wherever required
		for(i=0;i<stringLength;i++)
		{
			if(stringArray[i].equals("AND") || stringArray[i].equals("OR") || stringArray[i].equals("NOT"))
			{

			}
			else if(stringArray[i].equals("(") || stringArray[i].equals(")"))
			{

			}
			else if(!stringArray[i].contains(":"))
			{
				stringArray[i] = "Term:"+stringArray[i];
			}
		}//1. ends------------------------------------------------------------------


		//2. start------------------------------------------------------------------
		String term = "";
		String opr = "";

		//3. separating terms and operators
		for(i=0;i<stringLength;i++)
		{
			if(stringArray[i].contains(":"))
			{
				term = term + " " + stringArray[i];
				if(i!=stringLength-1)
					if(stringArray[i+1].contains(":") || stringArray[i+1].equals("("))
					{
						opr = opr + " " + defaultOperator;
					}
			}
			else if(stringArray[i].equals("(") || stringArray[i].equals(")"))
			{
				term = term + " " + stringArray[i];
			}
			else
			{
				opr = opr + " " + stringArray[i];
			}
		}//3. ends

		String tempTerm = term.replaceAll(":[a-zA-Z]+", "");		
		term  = term.replaceAll("  "," ");
		tempTerm = tempTerm.trim();

		String[] tempTermArray = tempTerm.split(" ");
		int j = 0;
		i = 0;
		int start=0;
		int end=0;

		//grouping in brackets same terms
		for(i=0;i<tempTermArray.length;i++){

			if(i+1<tempTermArray.length)
			{
				if(tempTermArray[i].equals(tempTermArray[i+1]))
				{
					start = i;
					for(j=i+1;j<tempTermArray.length;j++)
					{
						if(tempTermArray[i].equals(tempTermArray[j]))
						{
							end = j;
							i++;
						}
						else
							break;
					}
				}
			}
		}//ends for

		term = term.trim();
		tempTermArray = term.split(" ");
		if(end-start>1){
			tempTermArray[start] = "( " + tempTermArray[start];
			tempTermArray[end] = tempTermArray[end] + " )";
		}
		term="";
		for(i=0;i<tempTermArray.length;i++)
		{

			term = term + " " + tempTermArray[i];
		}
		term = term.trim();
		tempTermArray = term.split(" ");
		opr = opr.trim();
		String[] oprArray = opr.split(" ");
		String complete="";
		j=0;
		for(i=0;i<tempTermArray.length;i++)
		{
			complete = complete + " " + tempTermArray[i];
			if(i+1<tempTermArray.length)
				if(!tempTermArray[i].contains("(") && !tempTermArray[i+1].contains(")"))
					if(j<oprArray.length){
						complete = complete + " " + oprArray[j];
						j++;
					}
		}
		complete = complete.trim();

		return complete;
	}

	private static String handleNot(String complete)
	{
		int i = 0;
		String[] stringArray = complete.split(" ");
		complete="";
		for(i=0;i<stringArray.length;i++)
		{
			if(stringArray[i].equals("NOT"))
			{
				stringArray[i] = "AND";
				stringArray[i+1] = "<" + stringArray[i+1] +">";
			}
			complete = complete + " " + stringArray[i];
		}
		complete = complete.replace("(", "( ");
		complete = complete.replace(")", " )");
		complete = complete.replace("  ", " ");
		complete = complete.replace("  ", " ");
		complete = complete.trim();

		return complete;
	}
}




