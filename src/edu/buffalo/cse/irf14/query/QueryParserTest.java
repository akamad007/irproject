package edu.buffalo.cse.irf14.query;

import org.junit.* ;
import static org.junit.Assert.* ;

public class QueryParserTest {


	   @Test
	   public void test_1() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("hello world","OR").toString().equals("{ Term:hello OR Term:world }")) ;
	   }

	   @Test
	   public void test_2() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("orange AND yellow","OR").toString().equals("{ Term:orange AND Term:yellow }")) ;
	   }
	   
	   @Test
	   public void test_3() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("Category:War AND Author:Dutt AND Place:Baghdad AND prisoners detainees rebels","OR").toString().equals("{ Category:War AND Author:Dutt AND Place:Baghdad AND [Term:prisoners OR Term:detainees OR Term:rebels] }")) ;
	   }

	   @Test
	   public void test_4() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("(black OR blue) AND bruises","OR").toString().equals("{ [Term:black OR Term:blue] AND Term:bruises }")) ;
	   }
	
	   @Test
	   public void test_5() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("\"hello world\"","OR").toString().equals("{ Term:\"hello world\" }")) ;
	   }
	   
	   @Test
	   public void test_6() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("(Love NOT War) AND Category:(movies NOT crime)","OR").toString().equals("{ [Term:Love AND <Term:War>] AND [Category:movies AND <Category:crime>] }")) ;
	   }
	   @Test
	   public void test_7() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("fully convertible non convertible optionally convertible pct convertible debentures","OR").toString().equals("{ [Term:fully OR Term:convertible OR Term:non OR Term:convertible OR Term:optionally OR Term:convertible OR Term:pct OR Term:convertible OR Term:debentures] }")) ;
	   }
	   
	   @Test
	   public void test_8() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("place:tokyo NOT bank","OR").toString().equals("{ place:tokyo AND <Term:bank> }")) ;
	   }
	   @Test
	   public void test_9() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("factory workers lay-offs lockouts strikes","OR").toString().equals("{ Term:factory OR Term:workers OR Term:lay-offs OR Term:lockouts OR Term:strikes }")) ;
	   }
	   @Test
	   public void test_10() {
	      QueryParser S = new QueryParser() ;
	      assertTrue(S.parse("Category:oil AND place:Dubai AND ( price OR cost )","OR").toString().equals("place:tokyo NOT bank")) ;
	   }
	   
	   
	   
}
