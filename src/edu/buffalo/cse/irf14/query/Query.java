package edu.buffalo.cse.irf14.query;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents a parsed query
 * @author nikhillo
 *
 */
public class Query {
	/**
	 * Method to convert given parsed query into string
	 */
	private String query = "";
	private String postFix="";
	public List<String> rawTerms=null;
	Query(String q, String p){
		query = q;
		postFix = p;
		rawTerms = getTerms();
	}
	
	public String toPostFix() {
		//TODO: YOU MUST IMPLEMENT THIS
		return postFix;
	}
	public String toString() {
		//TODO: YOU MUST IMPLEMENT THIS
		return query;
	}
	private List<String> getTerms(){
		//System.out.println(postFix.toString());
		String[] tempArray = postFix.split(" ");
		List<String> terms = new ArrayList<String>();
		int i = 0;
		String term = "";
		for(i=0;i<tempArray.length;i++)
		{
			if(tempArray[i].contains("AND") || tempArray[i].contains("OR") || tempArray[i].contains("NOT"))
			{
				
			}
			else if(tempArray[i].contains(":"))
			{
				try{
					
					terms.add(tempArray[i].split(":")[1]);
				}
				catch(ArrayIndexOutOfBoundsException e){
					
				}
				//System.out.println(terms.toString());
			}
		}
		
		
		return terms;
	}
}
