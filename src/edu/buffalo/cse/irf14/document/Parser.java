/**
 * 
 */
package edu.buffalo.cse.irf14.document;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @author nikhillo
 * Class that parses a given file into a Document
 */
public class Parser {
	/**
	 * Static method to parse the given file into the Document object
	 * @param filename : The fully qualified filename to be parsed
	 * @return The parsed and fully loaded Document object
	 * @throws ParserException In case any error occurs during parsing
	 * @throws FileNotFoundException 
	 */
	static String pattern = Pattern.quote(System.getProperty("file.separator"));
	public static Document parse(String filename) throws ParserException {		
			if(filename!=null){				
				String FileID = null, Category, Title ="", Author="", AuthorOrg="", Place="", NewsDate=""
						, Content="";				   				
				Document d = new Document();
				try{
					FileID = regexFileId(filename);
					if(FileID!=null)		
					{
						Category = regexCategory(filename);						
						String s=null;	
						String [] temp;
						BufferedReader br;						
						try {													
							br = new BufferedReader(new FileReader(filename));								
							 while((s=br.readLine())!=null){
								if(!s.isEmpty()){						
									if(Title==""){
										Title = regexTitle(s,br);								
									}
										else {
											if(Author==""){	
												
												temp = regexAuthor(s);
												Author = temp[0];
												AuthorOrg = temp[1];	
												if (Author!=null){
													continue;
												}
											}
											if(Place==""){	
												temp = regexPlace(s);									
												
												if(temp.length>=3){
														Place = temp[0];
														NewsDate = temp[1];	
														Content +=temp[2];
													}
												else{
														Place = null;
														NewsDate=null;
														Content+=temp[0];																						
												}									
												continue;													
											}							
												Content+=" "+s;																	
										}
								}					 
							 }			
							
							 d.setField(FieldNames.FILEID, FileID);
							 d.setField(FieldNames.CATEGORY, Category);							
							 d.setField(FieldNames.TITLE, Title);
							 d.setField(FieldNames.AUTHOR, Author);					 
							 d.setField(FieldNames.AUTHORORG,AuthorOrg);
							 d.setField(FieldNames.PLACE, Place);
							 d.setField(FieldNames.NEWSDATE, NewsDate);
							 d.setField(FieldNames.CONTENT, Content);							 
							 br.close();						
						} catch (FileNotFoundException e){
							// TODO Auto-generated catch block
							
							e.printStackTrace();
							throw new ParserException(); 
							 
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							
						}						
					}	
				}
				catch(FileNotFoundException e){
					//e.printStackTrace();
					throw new ParserException();
				}
				return d;
				
			}
			else{
				throw new ParserException(); 
			}
	}
	public static String regexFileId (String FileName) throws  FileNotFoundException
	{		
				
		String simpleFileName [] = null;				
		simpleFileName = FileName.split(pattern);		
		if(simpleFileName.length>1){
			
		}		
		else{
			
			throw new FileNotFoundException();
		}
		
		return simpleFileName[simpleFileName.length-1];
	}
	public static String regexCategory(String FileName) throws ParserException{	
		try{
		String[] splittedFileName = FileName.split(pattern);
		String category = splittedFileName[splittedFileName.length-2];
		return category;}
		catch(Exception e){
			throw new ParserException();
		}
	}
	public static String regexTitle(String line, BufferedReader br) throws IOException {		
		if(line!=line.toUpperCase())
			{
				line=null;					
			}	
		else{
			String s = null;
			s = br.readLine();
			if(!s.isEmpty()&&s.toUpperCase()==s)
			{
				line+=s;
			}
		}		
		return line;
	}
	public static String [] regexAuthor(String line) throws ParserException{	
		try{
		String [] s = {null,null};
		if(line.contains("<AUTHOR>")){
					line =line.split("(\\W|^)<AUTHOR>(\\W|$)")[1].split("(</AUTHOR>)")[0].split("(By)|(BY)|(by)")[1];				
					s[0] = line.split(",")[0].trim();	
					try{
						s[1] = line.split(",")[1].trim();						
					}
					catch(ArrayIndexOutOfBoundsException e){						
					}
			}			 	
		return s;
		}
		catch(Exception e){
			throw new ParserException();
			
		}
	}
	
	public static String [] regexPlace(String line) throws ParserException{
		try{
		String []s = {null,null};
		String [] temp2 = {null,null,null};
		if(line.contains("-")&&line.contains(",")){			
			s = line.split("-");			
			if(s.length<=1){
				s[0] = line;
				line = null;				
			}
			else{
				if(s[0].lastIndexOf(",")>-1){
					temp2[0]= s[0].substring(0,s[0].lastIndexOf(",")).trim();	
					temp2[1] =  s[0].substring(s[0].lastIndexOf(",")+1).trim();
				}															 
				temp2[2] = s[1];				
			}
		}
		return temp2;
	}
	
	catch(Exception e){
		throw new ParserException();
	}
	}
}
