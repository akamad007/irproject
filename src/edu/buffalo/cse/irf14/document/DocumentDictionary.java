package edu.buffalo.cse.irf14.document;
import java.util.HashMap;

public class DocumentDictionary {
	
	private HashMap<String, String> map;
	private static DocumentDictionary singleton = new DocumentDictionary( );
	
	private DocumentDictionary() {
		map = new HashMap<String, String>();
	}
	
	public static DocumentDictionary getInstance() {
		//TODO: YOU NEED TO IMPLEMENT THIS METHOD
		return singleton;
	}
	
	public void setField(String fn, String o) {
		map.put(fn, o);		
	}
	public String getField(String fn) {
		return map.get(fn);
	}
}
