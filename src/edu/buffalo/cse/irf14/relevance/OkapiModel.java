package edu.buffalo.cse.irf14.relevance;


import java.util.Arrays;
import java.util.List;
import java.util.HashMap;
import java.util.Map.Entry;

public class OkapiModel {

	
	 

	public Double[] getRelevanceScoreSingleTerm(String term, List<Integer> tf, int df, int totalDocs,List<Integer> lD,int lDAvg,List<Integer> docList) {
		
		Double [] score = new Double[tf.size()];
		for(int i = 0;i<score.length;i++){
			score[i]=0.0;
		}
		double k1=2,b=0.75;
		
		for(int i=0;i<tf.size();i++){
			Integer someTf = tf.get(i);
			
			if(df!=0&&someTf!=null)
				score[i] = ((Math.log10(totalDocs/df)*(k1+1)*someTf)/(k1*((1-b)+b*(lD.get(i)/lDAvg))+someTf));				
			
		}
		return score;
	}
	
	private int getQueryTermFrequency(String term,List<String> query){
		int count = 0;
		for(String q:query){
			if(q==term){
				count=count+1;
			}
		}
		return count;
	}
	

	public HashMap<Integer, Double> getRelevanceScore(List<String> terms,List<Integer> docList, HashMap<String,List<Integer>> tf,
			List<Integer> df, int totalDocs,List<Integer> lD,int lDAvg) {
		
		Double[] temp;
		HashMap<String,Double []> list = new HashMap<String,Double []>();
		HashMap<Integer,Double > result = new HashMap<Integer,Double>();
		Double [] sum = new Double[docList.size()];
		for(int i=0;i<sum.length;i++){
			sum[i]=0.0;
		}
		int size = terms.size();		
		String term;
		int tfQ;		
		
		for(int i=0;i<size;i++){
				term = terms.get(i);
			
					
					tfQ= getQueryTermFrequency(terms.get(i),terms);				
					temp =getRelevanceScoreLong(term,tfQ,tf.get(term), df.get(i), totalDocs,lD,lDAvg,docList);
				
				list.put(term, temp);
				
			}
		for(Entry<String, Double[]> h :list.entrySet())
			{
				Double [] value = h.getValue();
				for(int j = 0;j<docList.size();j++){
					try{
						
						sum[j] =sum[j]+value[j];
					}
					catch(ArrayIndexOutOfBoundsException e){
						
					}
				}
			}
	
		Double [] temp1 = sum.clone();
		Arrays.sort(temp1);
		Double max = temp1[temp1.length-1];
		for(int i =0;i<docList.size();i++){		
			
			sum[i] = (double)Math.round((sum[i]/(max)) * 10) / 10;
			result.put(docList.get(i), sum[i]);
		}
		return result;
	}
	
	private  Double [] getRelevanceScoreLong(String term, int tfQ,List<Integer> tf, int df, int totalDocs,List<Integer> lD,int lDAvg,List<Integer> docList) {
		
		Double score[] = new Double[tf.size()];
		for(int i = 0;i<score.length;i++){
			score[i]=0.0;
		}
		double k1=1.8,b=0.75,k3 =2;
		for(int i=0;i<tf.size();i++){
			Integer someTf = tf.get(i);
		
			if(df!=0&&(someTf!=null))
			score[i] = (((Math.log10(totalDocs/df)*(k1+1)*someTf)/(k1*((1-b)+b*(lD.get(i)/lDAvg))+someTf))*((k3+1)*tfQ/(k3+tfQ)));
			
		}
		return score;	
	}
}	
	