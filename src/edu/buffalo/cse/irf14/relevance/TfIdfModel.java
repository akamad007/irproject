package edu.buffalo.cse.irf14.relevance;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class TfIdfModel {

	private Double[] getTfIdfScore(int [] tf,Integer totalDocs,List<Integer> df){
		Double [] result = new Double[tf.length] ;
		
		for(int i = 0;i< tf.length;i++){			
			if(df.get(i)==0)
				result[i] = 0.0;
			else
				result[i]= Math.log10(totalDocs/df.get(i))*tf[i];	
			
		}
		
		return result;
	}
	private Double[] getDocumentScore(List<Integer >tf){
		Double [] result = new Double[tf.size()] ;
		double res = 0.0;
		for(int i=0;i<tf.size();i++)
		{
			if(tf.get(i)!=null)
				res = res+tf.get(i)*tf.get(i);			
		}
		res = Math.sqrt(res);
		for(int i=0;i<tf.size();i++)
		{
			if(tf.get(i)!=null&&res!=0)
			result[i] = tf.get(i)/res;
			else
			result[i]=0.0;
		
		}
		
		return result;
	}
	
	public HashMap<Integer, Double> getCosineScore(List<String> terms,List<Integer> docList, HashMap<String,List<Integer>> tf,
			List<Integer> df, int totalDocs ){
			HashMap<String,Double []> list = new HashMap<String,Double []>();
			int [] queryTf = new int[terms.size()] ;
			Double [] sum = new Double[docList.size()];
			Double [] finalSum = new Double[docList.size()];
		
			for(int i=0;i<terms.size();i++){
				queryTf[i] = getQueryTermFrequency(terms.get(i),terms);
			}
			for(int i=0;i<docList.size();i++){
				sum[i] = 0.0;
				finalSum[i] = 0.0;
			}
		
			Double [] queryResult = getTfIdfScore(queryTf,totalDocs,df);
			Double []res =  new Double[docList.size()];
			for(int i=0;i<terms.size();i++){
				res = getDocumentScore(tf.get(terms.get(i)));				
				list.put(terms.get(i), res);
				
			}
			for(Entry<String, Double[]> h:list.entrySet()){
				Double []value = h.getValue();
				
				for(int j=0;j<docList.size();j++){
					sum[j] =sum[j]+value[j];
					
				}
			}
			
			for(int i=0;i<queryResult.length;i++){
				for(int j=0;j<docList.size();j++){
					
					finalSum[j] = finalSum[j]+queryResult[i]*sum[j];
				}
			}
			
			Double [] temp1 = finalSum.clone();
			Arrays.sort(temp1);
			Double max = temp1[temp1.length-1];
			
			
			HashMap<Integer,Double> finalResult = new HashMap<Integer,Double>();
			for(int i =0;i<docList.size();i++){		
				
				finalSum[i] = (double)Math.round((finalSum[i]/(max)) * 10) / 10;
				finalSum[i] = finalSum[i]+.15;
				if(finalSum[i]>1.0){
					finalSum[i] = 1.0;
				}
				finalSum[i] = (double)Math.round((finalSum[i]) * 10) / 10;
				finalResult.put(docList.get(i), finalSum[i]);
			}						
			return finalResult;		
	}
	private int getQueryTermFrequency(String term,List<String> query){
		int count = 0;
		for(String q:query){
			if(q==term){
				count=count+1;
			}
		}
		return count;
	}
	
	
}
