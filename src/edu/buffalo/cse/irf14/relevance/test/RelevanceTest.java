/*package edu.buffalo.cse.irf14.relevance.test;

import static org.junit.Assert.assertEquals;


import java.util.ArrayList;
import java.util.HashMap;


import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.buffalo.cse.irf14.analysis.Analyzer;
import edu.buffalo.cse.irf14.analysis.AnalyzerFactory;
import edu.buffalo.cse.irf14.analysis.TokenStream;
import edu.buffalo.cse.irf14.analysis.Tokenizer;
import edu.buffalo.cse.irf14.analysis.TokenizerException;
import edu.buffalo.cse.irf14.document.Document;
import edu.buffalo.cse.irf14.document.FieldNames;
import edu.buffalo.cse.irf14.index.IndexReader;
import edu.buffalo.cse.irf14.index.IndexType;
import edu.buffalo.cse.irf14.index.IndexWriter;
import edu.buffalo.cse.irf14.index.IndexerException;
import edu.buffalo.cse.irf14.query.QueryType;
import edu.buffalo.cse.irf14.relevance.OkapiModel;

public class RelevanceTest {
	private IndexReader reader;
	
	@BeforeClass
	public final static void setupIndex() throws IndexerException {
		String[] strs = {"new home sales top sales forecasts", "home sales rise in july", 
				"increase in home sales in july", "july new home sales rise"};
		int len = strs.length;
		Document d;
		String dir = "E:\\Studies PDF Sem 1\\CSE - 535 Information Retrieval\\Project\\Indexes";
		IndexWriter writer = new IndexWriter(dir); //set this beforehand
		for (int i = 0; i < len; i++) {
			d = new Document();
			d.setField(FieldNames.FILEID, "0000"+(i+1));
			d.setField(FieldNames.CONTENT, strs[i]);
			writer.addDocument(d);
		}
		
		writer.close();
	}

	@Before
	public final void before() {
		reader = new IndexReader("E:\\Studies PDF Sem 1\\CSE - 535 Information Retrieval\\Project\\Indexes", IndexType.TERM);
	}

	private static String getAnalyzedTerm(String string) {
		Tokenizer tknizer = new Tokenizer();
		AnalyzerFactory fact = AnalyzerFactory.getInstance();
		try {
			TokenStream stream = tknizer.consume(string);
			Analyzer analyzer = fact.getAnalyzerForField(FieldNames.CONTENT, stream);
			
			while (analyzer.increment()) {
				
			}
			stream = analyzer.getStream();
			stream.reset();
			return stream.next().toString();
		} catch (TokenizerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Test
	public void testOkapi(){
		OkapiModel o = new OkapiModel();
		ArrayList<String> vals = new ArrayList<String>();
		vals.add("sales"); vals.add("home");vals.add("forecasts");
		ArrayList<String> docList = new ArrayList<String>();
		docList.add("0001");
		docList.add("0002");docList.add("0003");docList.add("0004");
		ArrayList<Integer> tfList = new ArrayList<Integer>();
		HashMap<String,ArrayList<Integer>> tf = new HashMap<String,ArrayList<Integer>>();
		HashMap<String,Integer> df = new HashMap<String,Integer>();
		ArrayList<Integer> lD = new ArrayList<Integer> ();
		
		df.put(vals.get(0), 4);df.put(vals.get(1), 4);df.put(vals.get(2), 1);
		tfList.add(2);tfList.add(1);tfList.add(1);tfList.add(1);
		tf.put(vals.get(0),tfList );
		
		tfList.clear();
		tfList.add(1);tfList.add(1);tfList.add(1);tfList.add(1);
		tf.put(vals.get(1),tfList );
		
		tfList.clear();
		tfList.add(1);	
		tf.put(vals.get(2),tfList );
		
		lD.add(6);lD.add(4);lD.add(6);lD.add(5);
		int lDAvg = 20/4;
		HashMap<String,Double> check = o.getRelevanceScore(vals,docList , tf, df, 4, lD, lDAvg);
		int i =0;
		


		
		
		
		
		
		
		
		
		
		i++;
		assertEquals(check.get(docList.get(i)),0.0,0.0);
		i++;
		assertEquals(check.get(docList.get(i)),0.0,0.0);
		i++;
		assertEquals(check.get(docList.get(i)),0.0,0.0);
		
		
		
	}

}
*/