package edu.buffalo.cse.irf14.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.buffalo.cse.irf14.SearchRunner;
import edu.buffalo.cse.irf14.analysis.Analyzer;
import edu.buffalo.cse.irf14.analysis.AnalyzerFactory;
import edu.buffalo.cse.irf14.analysis.TokenStream;
import edu.buffalo.cse.irf14.analysis.Tokenizer;
import edu.buffalo.cse.irf14.analysis.TokenizerException;
import edu.buffalo.cse.irf14.document.Document;
import edu.buffalo.cse.irf14.document.FieldNames;
import edu.buffalo.cse.irf14.index.IndexReader;
import edu.buffalo.cse.irf14.index.IndexType;
import edu.buffalo.cse.irf14.index.IndexWriter;
import edu.buffalo.cse.irf14.index.IndexerException;
import edu.buffalo.cse.irf14.query.QueryType;

public class TestSearchRunner {
	
	

	
	/**
	 * Test method for {@link edu.buffalo.cse.irf14.index.IndexReader#getTopK(int)}.
	 */
	@Test
	public final void searchRunnerTest() {
		String indexDir = "H:\\Fall 2014\\Information Retrieval\\project1\\irproject\\training";
		String flatDir = "H:\\Fall 2014\\Information Retrieval\\project1\\irproject\\flat";
		PrintStream baos = System.out;
		SearchRunner search  = new SearchRunner(indexDir,flatDir,'Q',baos);
	
	}
	

}
