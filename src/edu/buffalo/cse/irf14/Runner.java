/**
 * 
 */
package edu.buffalo.cse.irf14;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import edu.buffalo.cse.irf14.document.Document;
import edu.buffalo.cse.irf14.document.Parser;
import edu.buffalo.cse.irf14.document.ParserException;
import edu.buffalo.cse.irf14.index.IndexWriter;
import edu.buffalo.cse.irf14.index.IndexerException;
import edu.buffalo.cse.irf14.SearchRunner;
import edu.buffalo.cse.irf14.SearchRunner.ScoringModel;

/**
 * @author nikhillo
 *
 */
public class Runner {

	/**
	 * 
	 */
	public Runner() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String ipDir = args[0];
		String indexDir = args[1];
		//more? idk!
		
		File ipDirectory = new File(ipDir);
	
		String[] catDirectories = ipDirectory.list();
		
		String[] files;
		File dir;
		
		Document d = null;
		IndexWriter writer = new IndexWriter(indexDir);
		
		try {
			
			for (String cat : catDirectories) {
				dir = new File(ipDir+ File.separator+ cat);
				files = dir.list();				
				if (files == null)
					continue;		
				
				for (String f : files) {
					try {
						d = Parser.parse(dir.getAbsolutePath() + File.separator +f);
						writer.addDocument(d);
					} catch (ParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 					
				}	
						
			}
		
			writer.close();
			

			String flatDir = "C:\\Users\\akash\\workspace\\IRproject\\flat";//"H:\\Fall 2014\\Information Retrieval\\project1\\irproject\\flat";
			PrintStream output = System.out;//new PrintStream("C:\\Users\\akash\\workspace\\IRproject\\ModeEOutput.txt");

			
			
			
			SearchRunner search  = new SearchRunner(indexDir,flatDir,'Q',output);			

			search.query("mitsubishi", ScoringModel.OKAPI);			
			File f = new File("C:\\Users\\akash\\workspace\\IRproject\\ModeE.txt");

			SearchRunner search1  = new SearchRunner(indexDir,flatDir,'E',output);//change the stream 
			search1.query(f);
			
		} catch(Exception e){// (IndexerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
